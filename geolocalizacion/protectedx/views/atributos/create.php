<?php
/* @var $this AtributosController */
/* @var $model Atributos */

$this->breadcrumbs=array(
	'Atributoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Atributos', 'url'=>array('index')),
	array('label'=>'Manage Atributos', 'url'=>array('admin')),
);
?>

<h1>Create Atributos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>