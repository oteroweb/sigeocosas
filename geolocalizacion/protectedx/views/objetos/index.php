<?php
/* @var $this ObjetosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Objetoses',
);

$this->menu=array(
	array('label'=>'Create Objetos', 'url'=>array('create')),
	array('label'=>'Manage Objetos', 'url'=>array('admin')),
);
?>

<h1>Lista de Objetos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
