<?php
/* @var $this ObjetosController */
/* @var $model Objetos */
/* @var $form CActiveForm */
?>

<div class="form">
	<div class="gllpLatlonPicker">
    <fieldset class="gllpLatlonPicker">
	<br/> <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'objetos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>


<?php if ($model->isNewRecord!='1'): ?>

	<div class="row">
     <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->imagen1,"imagen",array("width"=>200)); ?>
	</div>
	<div class="row">
     <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->imagen2,"imagen",array("width"=>200)); ?>
	</div>
	<div class="row">
     <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->imagen2,"imagen",array("width"=>200)); ?>
	</div>
	<?php // else: ?>
	<?php endif ?>
		<div class="row">
			<?= $form->labelEx($model,'imagen1').CHtml::activeFileField($model, 'imagen1').$form->error($model,'imagen1'); ?>
		</div>
		<div class="row">
			<?= $form->labelEx($model,'imagen2').CHtml::activeFileField($model, 'imagen2').$form->error($model,'imagen2'); ?>
		</div>
		<div class="row">
			<?= $form->labelEx($model,'imagen3').CHtml::activeFileField($model, 'imagen3').$form->error($model,'imagen3'); ?>
		</div>
		

</div>
<input type="text" class="gllpSearchField" />

<input type="button" class="gllpSearchButton" value="search">

	<div class="gllpMap" id="map_canvas">
	</div>
		<?php echo $form->labelEx($model,'latitud'); ?>
		<?php echo $form->textField($model,'latitud',array('size'=>40,'maxlength'=>40, 'value'=> $model->latitud, 'class'=>'gllpLatitude')); ?>
		<?php echo $form->error($model,'latitud'); ?>
	</br>

		<?php echo $form->labelEx($model,'longitud'); ?>
		<?php echo $form->textField($model,'longitud',array('size'=>40,'maxlength'=>40,'value'=> $model->longitud, 'class'=>'gllpLongitude')); ?>
		<?php echo $form->error($model,'longitud'); ?>
</br>
		<?php echo $form->labelEx($model,'zoom'); ?>
		<?php echo $form->textField($model,'zoom',array('size'=>40,'maxlength'=>40,'value'=> $model->zoom, 'class'=>'gllpZoom')); ?>
		<?php echo $form->error($model,'zoom'); ?>


	<div class="row">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>


	

<?php $this->endWidget(); ?>
</fieldset>
  </div>

<script type="text/javascript">

function initialize() {
    var latlng = new google.maps.LatLng(-34.397, 150.644);
    var myOptions = {
        zoom: 7,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"),
            myOptions);
}
google.maps.event.addDomListener(window, "load", initialize);

</script>
</div><!-- form -->


