<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'documentos-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'enableClientValidation'=>true,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="text-danger">Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php //echo $form->textFieldRow($model,'documento',array('class'=>'form-control','maxlength'=>255)); ?>
	<?php //echo $form->textAreaRow($model,'descripcion',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
	<?php echo $form->textFieldRow($model,'idserie',array('class'=>'form-control')); ?>
	<?php echo $form->textFieldRow($model,'idsubserie',array('class'=>'form-control')); ?>
	<?php echo '<div class="form-group has-feedback">'.$form->labelEx($model, 'archivo',array('class'=>'control-label col-sm-3'));  echo '<div class="col-sm-3"> '.$form->fileField($model,'archivo',array('class'=>'form-control'));  echo $form->error($model, 'archivo').'</div></div>' ; ?>

<!-- <p class="bg-danger">Nueva sección </p>-->

	<div class="form-group">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'danger',
			'label'=>$model->isNewRecord ? 'Guardar' : 'Guardar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
<script>
  jQuery(function() {

    jQuery('.date').datetimepicker({
      format: 'YYYY-MM-DD'
    });

    jQuery('.chosen-select').chosen({search_contains: true });
    jQuery('.chosen-select-deselect').chosen({ allow_single_deselect: true, search_contains: true, width: "100%" });
  });
</script>