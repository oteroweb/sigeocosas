<div class="panel panel-warning">
<div class="panel-heading">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

</div>
<div class="panel-body">
	<b><?php echo CHtml::encode($data->getAttributeLabel('documento')); ?>:</b>
	<?php echo CHtml::encode($data->documento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idserie')); ?>:</b>
	<?php echo CHtml::encode($data->idserie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idsubserie')); ?>:</b>
	<?php echo CHtml::encode($data->idsubserie); ?>
	<br />

	<b><?php /*echo CHtml::encode($data->getAttributeLabel('archivo')); ?>:</b>
	<?php echo CHtml::encode($data->archivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuariocrea')); ?>:</b>
	<?php echo CHtml::encode($data->usuariocrea); ?>
	<br />

	<?php 
	<b><?php echo CHtml::encode($data->getAttributeLabel('fechacrea')); ?>:</b>
	<?php echo CHtml::encode($data->fechacrea); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuariomodifica')); ?>:</b>
	<?php echo CHtml::encode($data->usuariomodifica); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechamodifica')); ?>:</b>
	<?php echo CHtml::encode($data->fechamodifica); ?>
	<br />

	*/ ?>
</div>
</div>