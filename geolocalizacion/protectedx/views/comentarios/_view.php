<?php
/* @var $this ComentariosController */
/* @var $data Comentarios */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idcomentario')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idcomentario), array('view', 'id'=>$data->idcomentario)); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('comentario')); ?>:</b>
	<?php echo CHtml::encode($data->comentario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idobjeto')); ?>:</b>
	<?php echo CHtml::encode($data->idobjeto0->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idusuario')); ?>:</b>
	<?php echo CHtml::encode($data->idusuario0->nombre); ?>
	<br />


</div>