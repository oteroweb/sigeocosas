<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Login</h1>

<p>ingresar datos de acceso.</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">los campos con  <span class="required">*</span> son requeridos.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
		
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>
	

<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
			<br>
		<?php $this->widget('CCaptcha'); ?>
		 <br>
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>

		
	<?php endif; ?>
	
	<div class="row ">
		<?php echo CHtml::submitButton('login', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

    

</div><!-- form -->
 
	




 <a href="<?php echo yii::app()->createUrl('site/recuperarpassword');?>">
	recuperar password
</a> <?php 
