<?php
/* @var $this CalificacionesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Calificaciones',
);

$this->menu=array(
	array('label'=>'Create Calificaciones', 'url'=>array('create')),
	array('label'=>'Manage Calificaciones', 'url'=>array('admin')),
);
?>

<h1>Lista de Calificaciones</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
