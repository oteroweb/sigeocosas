<?php
/* @var $this CalificacionesController */
/* @var $model Calificaciones */

$this->breadcrumbs=array(
	'Calificaciones'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Calificaciones', 'url'=>array('index')),
	array('label'=>'Manage Calificaciones', 'url'=>array('admin')),
);
?>

<h1>Create Calificaciones</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>