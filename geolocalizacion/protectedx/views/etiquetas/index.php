<?php
/* @var $this EtiquetasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Etiquetases',
);

$this->menu=array(
	array('label'=>'Create Etiquetas', 'url'=>array('create')),
	array('label'=>'Manage Etiquetas', 'url'=>array('admin')),
);
?>

<h1>Lista de Etiquetas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
