<?php
/* @var $this EtiquetasController */
/* @var $model Etiquetas */

$this->breadcrumbs=array(
	'Etiquetases'=>array('index'),
	$model->idetiqueta,
);

$this->menu=array(
	array('label'=>'List Etiquetas', 'url'=>array('index')),
	array('label'=>'Create Etiquetas', 'url'=>array('create')),
	array('label'=>'Update Etiquetas', 'url'=>array('update', 'id'=>$model->idetiqueta)),
	array('label'=>'Delete Etiquetas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idetiqueta),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Etiquetas', 'url'=>array('admin')),
);
?>

<h1>View Etiquetas #<?php echo $model->idetiqueta; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idetiqueta',
		'nombre',
	),
)); ?>
