<?php
/* @var $this ObjetoetiquetaController */
/* @var $model Objetoetiqueta */

$this->breadcrumbs=array(
	'Objetoetiquetas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Objetoetiqueta', 'url'=>array('index')),
	array('label'=>'Manage Objetoetiqueta', 'url'=>array('admin')),
);
?>

<h1>Create Objetoetiqueta</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>