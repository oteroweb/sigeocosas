<?php
/* @var $this ObjetoetiquetaController */
/* @var $model Objetoetiqueta */

$this->breadcrumbs=array(
	'Objetoetiquetas'=>array('index'),
	$model->idobjeetiq=>array('view','id'=>$model->idobjeetiq),
	'Update',
);

$this->menu=array(
	array('label'=>'List Objetoetiqueta', 'url'=>array('index')),
	array('label'=>'Create Objetoetiqueta', 'url'=>array('create')),
	array('label'=>'View Objetoetiqueta', 'url'=>array('view', 'id'=>$model->idobjeetiq)),
	array('label'=>'Manage Objetoetiqueta', 'url'=>array('admin')),
);
?>

<h1>Update Objetoetiqueta <?php echo $model->idobjeetiq; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>