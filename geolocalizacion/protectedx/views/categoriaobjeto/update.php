<?php
/* @var $this CategoriaobjetoController */
/* @var $model Categoriaobjeto */

$this->breadcrumbs=array(
	'Categoriaobjetos'=>array('index'),
	$model->idcateobje=>array('view','id'=>$model->idcateobje),
	'Update',
);

$this->menu=array(
	array('label'=>'List Categoriaobjeto', 'url'=>array('index')),
	array('label'=>'Create Categoriaobjeto', 'url'=>array('create')),
	array('label'=>'View Categoriaobjeto', 'url'=>array('view', 'id'=>$model->idcateobje)),
	array('label'=>'Manage Categoriaobjeto', 'url'=>array('admin')),
);
?>

<h1>Update Categoriaobjeto <?php echo $model->idcateobje; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>