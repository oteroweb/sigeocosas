<?php

/**
 * This is the model class for table "sicomparendo.documentos".
 *
 * The followings are the available columns in table 'sicomparendo.documentos':
 * @property string $id
 * @property string $documento
 * @property string $descripcion
 * @property integer $idserie
 * @property integer $idsubserie
 * @property string $archivo
 * @property string $usuariocrea
 * @property string $fechacrea
 * @property string $usuariomodifica
 * @property string $fechamodifica
 *
 * The followings are the available model relations:
 * @property Incidenteprocessacciondocumento[] $incidenteprocessacciondocumentos
 */
class Documentos extends CActiveRecord
{
	private static $dboperar = NULL;
	public static $arrayTipos = array( 'id'=>'id' ,  'documento'=>'documento' ,  'descripcion'=>'descripcion' ,  'idserie'=>'idserie' ,  'idsubserie'=>'idsubserie' ,  'archivo'=>'archivo' ,  'usuariocrea'=>'usuariocrea' ,  'fechacrea'=>'fechacrea' ,  'usuariomodifica'=>'usuariomodifica' ,  'fechamodifica'=>'fechamodifica'  );
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Documentos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'documentos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('documento, descripcion', 'required'),
			array('idserie, idsubserie', 'numerical', 'integerOnly'=>true),
			array('documento', 'length', 'max'=>255),
			array('usuariocrea, usuariomodifica', 'length', 'max'=>15),
			array('archivo, fechacrea, fechamodifica', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, documento, descripcion, idserie, idsubserie, archivo, usuariocrea, fechacrea, usuariomodifica, fechamodifica', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'incidenteprocessacciondocumentos' => array(self::HAS_MANY, 'Incidenteprocessacciondocumento', 'iddocumento'),
		);
	}
	
	/**
	 * @return array list "sicomparendo.documentos".
	 */
	public static function getListDocumentos()
	{
		return CHtml::listData(Documentos::model()->findAll(),'id','concate');
	}
    
	/**
	 * @return array list "sicomparendo.documentos".
	 */
	public static function getDocumentos($id)
	{
		return CHtml::listData(Documentos::model()->findByPk($id),'id','concate');
	}

	public function getConcate()
	{
		return ( $this->id.': '.  $this->documento.': '.  $this->descripcion.': '.  $this->idserie.': '.  $this->idsubserie.': '.  $this->archivo.': '.  $this->usuariocrea.': '.  $this->fechacrea.': '.  $this->usuariomodifica.': '.  $this->fechamodifica.': ' );
	}
	
	public static function getListTipos($key=null)
	{
		if($key!=null)
			return self::$arrayTipos[$key];
		return self::$arrayTipos;
	}
	
	/**
	 * @return entero modelo para consultas complejas
	 */
	public static function getTotalDocumentos($id)
	{
		$count = Yii::app()->db->createCommand('SELECT COUNT(*) FROM sicomparendo.documentos  WHERE 1 =:id');
		$count->bindParam(':id', $id, PDO::PARAM_STR);
		$total = $count->queryScalar();
		return $total;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'documento' => 'Documento',
			'descripcion' => 'Descripcion',
			'idserie' => 'Idserie',
			'idsubserie' => 'Idsubserie',
			'archivo' => 'Archivo',
			'usuariocrea' => 'Usuariocrea',
			'fechacrea' => 'Fechacrea',
			'usuariomodifica' => 'Usuariomodifica',
			'fechamodifica' => 'Fechamodifica',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('documento',$this->documento,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('idserie',$this->idserie);
		$criteria->compare('idsubserie',$this->idsubserie);
		$criteria->compare('archivo',$this->archivo,true);
		$criteria->compare('usuariocrea',$this->usuariocrea,true);
		$criteria->compare('fechacrea',$this->fechacrea,true);
		$criteria->compare('usuariomodifica',$this->usuariomodifica,true);
		$criteria->compare('fechamodifica',$this->fechamodifica,true);
		/**
		 * if(Yii::app()->user->checkAccess('Administrador'))
		 *{
		 *	$criteria->with = array('participantes'=>array('select'=>'participantes.idTransportadoras','together'=>true));
		 *	$criteria->compare('participantes.idTransportadoras',Yii::app()->user->getState('idTransportador'),true);
		 *	$criteria->with = array('devolucion.prestamo.usuarioMateria');
		 *	$criteria->compare('usuarioMateria.idUsuario',Yii::app()->user->id,true);  
		 *}
		 *
		 */
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	  
}