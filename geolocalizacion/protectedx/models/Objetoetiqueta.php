<?php

/**
 * This is the model class for table "objetoetiqueta".
 *
 * The followings are the available columns in table 'objetoetiqueta':
 * @property integer $idobjeetiq
 * @property integer $idetiqueta
 * @property integer $idobjeto
 *
 * The followings are the available model relations:
 * @property Objetos $idobjeto0
 * @property Etiquetas $idetiqueta0
 */
class Objetoetiqueta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'objetoetiqueta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idetiqueta, idobjeto', 'required','message'=>'por  favor ingresar: {attribute}'), 
			array('idetiqueta, idobjeto', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idobjeetiq, idetiqueta, idobjeto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idobjeto0' => array(self::BELONGS_TO, 'Objetos', 'idobjeto'),
			'idetiqueta0' => array(self::BELONGS_TO, 'Etiquetas', 'idetiqueta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idobjeetiq' => 'Idobjeetiq',
			'idetiqueta' => 'Idetiqueta',
			'idobjeto' => 'Idobjeto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idobjeetiq',$this->idobjeetiq);
		$criteria->compare('idetiqueta',$this->idetiqueta);
		$criteria->compare('idobjeto',$this->idobjeto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Objetoetiqueta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
