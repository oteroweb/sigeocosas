<?php

/**
 * This is the model class for table "calificaciones".
 *
 * The followings are the available columns in table 'calificaciones':
 * @property integer $idcalificacion
 * @property integer $calificacion
 * @property integer $idobjeto
 * @property integer $idusuario
 *
 * The followings are the available model relations:
 * @property Objetos $idobjeto0
 * @property Usuarios $idusuario0
 */
class Calificaciones extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'calificaciones';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('calificacion, idobjeto, idusuario', 'required','message'=>'por  favor ingresar: {attribute}'), 
			array('calificacion, idobjeto, idusuario', 'numerical', 'integerOnly'=>true,'max'=>10,'min'=>1,'tooBig'=>'calificacion menor a 10','tooSmall'=>'calificacion mayor a 0'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idcalificacion, calificacion, idobjeto, idusuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idobjeto0' => array(self::BELONGS_TO, 'Objetos', 'idobjeto'),
			'idusuario0' => array(self::BELONGS_TO, 'Usuarios', 'idusuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idcalificacion' => 'Idcalificacion',
			'calificacion' => 'Calificacion',
			'idobjeto' => 'Idobjeto',
			'idusuario' => 'Idusuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idcalificacion',$this->idcalificacion);
		$criteria->compare('calificacion',$this->calificacion);
		$criteria->compare('idobjeto',$this->idobjeto);
		$criteria->compare('idusuario',$this->idusuario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Calificaciones the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
