<?php


class RecuperarPassword extends CFormModel {
    public $email;
    public $username;
    public $captcha;
    
    public function rules(){
        return array(
             array('email, username, captcha',
            'required',
           'message' => 'el campo es requerido'
                 ),
            
            array(
                'email',
                'email',
                 'message' => 'formato incorrecto DE EMAIL',
                ),
            array(
              'username',
                'match',
                'pattern' => '/^[a-z0-9 ����������������\_]+$/i',
                'message' => 'Error,solo letras, numeros y quiones bajos'
            ),
            array(
                 'captcha', 
                 'captcha', 
                 'message' => 'el codigo no es valido',
                    ), 
            
        
        );
        
    }
    public function attributeLabels()
            {
        return array(
            'username' => 'nombre de usuarioSss',
        );
        
    }
}
