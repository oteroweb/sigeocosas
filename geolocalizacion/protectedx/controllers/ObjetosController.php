<?php
Yii::import('ext.gmap.*');
class ObjetosController extends Controller
{
	

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','desactivar'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','create','update','delete'),
				'roles'=>array('admin'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','create','update','delete'),
				'roles'=>array('super'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','create','update','delete'),
				'users'=>array('jaco'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Objetos;
		if(isset($_POST['Objetos']))
		{
			$model->attributes=$_POST['Objetos'];
			$rnd1 = rand(0,9999);
			$rnd2 = rand(0,9999);
			$rnd3 = rand(0,9999); 
            $uploadedFile1=CUploadedFile::getInstance($model,'imagen1');
            $uploadedFile2=CUploadedFile::getInstance($model,'imagen2');
            $uploadedFile3=CUploadedFile::getInstance($model,'imagen3');
           	$fileName1 = "{$rnd1}-{$uploadedFile1}"; $fileName2 = "{$rnd2}-{$uploadedFile2}"; $fileName3 = "{$rnd3}-{$uploadedFile3}";
			if (empty($uploadedFile1)) {
 				$fileName1 = 'default.jpg'; 
			}
			if (empty($uploadedFile2)) {
 				$fileName2 = 'default.jpg'; 
			}
			if (empty($uploadedFile3)) {
 				$fileName3 = 'default.jpg'; 
			}
            $model->imagen1 = $fileName1;
            $model->imagen2 = $fileName2;
            $model->imagen3 = $fileName3;
        }
			if($model->save()) {
				if(!empty($uploadedFile1)) {
   					$uploadedFile1->saveAs(Yii::app()->basePath.'/../images/'.$fileName1);
				}
				if(!empty($uploadedFile2)) {
   					$uploadedFile2>saveAs(Yii::app()->basePath.'/../images/'.$fileName2);
				}
				if(!empty($uploadedFile)) {
   					$uploadedFile3->saveAs(Yii::app()->basePath.'/../images/'.$fileName3);
				}

				$this->redirect(array('view','id'=>$model->idobjeto));

			}
		

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $oldFileName1 = $model->imagen1;
        $oldFileName2 = $model->imagen2;
        $oldFileName3 = $model->imagen3;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Objetos']))
		{
			$model->attributes=$_POST['Objetos'];
			$uploadedFile1=CUploadedFile::getInstance($model,'imagen1');
			$uploadedFile2=CUploadedFile::getInstance($model,'imagen2');
			$uploadedFile3=CUploadedFile::getInstance($model,'imagen3');

			if ($uploadedFile1 !== null) {
				$model->imagen1 = $uploadedFile1;
			} else {
				$model->imagen1 = $oldFileName1;
			}
			if ($uploadedFile2 !== null) {
				$model->imagen2 = $uploadedFile2;
			} else {
				$model->imagen2 = $oldFileName2;
			}
			if ($uploadedFile3 !== null) {
				$model->imagen3 = $uploadedFile3;
			} else {
				$model->imagen3 = $oldFileName3;
			}



			if($model->save())
				  if(!empty($uploadedFile1)) { $uploadedFile1->saveAs(Yii::app()->basePath.'/../images/'.$model->imagen1); }
				  if(!empty($uploadedFile2)) { $uploadedFile2>saveAs(Yii::app()->basePath.'/../images/'.$model->imagen2); }
				  if(!empty($uploadedFile3)) { $uploadedFile3>saveAs(Yii::app()->basePath.'/../images/'.$model->imagen3); }				
				$this->redirect(array('view','id'=>$model->idobjeto));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
	
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{//urgente codigo tentativo para la consulta sql  ALTER TABLE public.objetos ADD COLUMN status boolean; ALTER TABLE public.objetos ALTER COLUMN status SET DEFAULT true;

// <?php echo CHtml::link(CHtml::encode($data->nombre), array('view', 'id'=>$data->idobjeto)); 


		$dataProvider=new CActiveDataProvider('Objetos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Objetos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Objetos']))
			$model->attributes=$_GET['Objetos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Objetos the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Objetos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


public function actionDesactivar($id)
	{
		$model=Objetos::model()->findByPk($_GET['id']);
		// var_dump(expression)
		if ($model->status == true) {
		$model->status = false;
		echo "ta";
			$model->save();

		}
		else {
			echo "nota";
			$model->status = true;
			$model->save();
		}
		$this->redirect(array('/objetos'));
		// if($model===null)
			// throw new CHttpException(404,'The requested page does not exist.');
		// return $model;
	}
	/**
	 * Performs the AJAX validation.
	 * @param Objetos $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='objetos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}
