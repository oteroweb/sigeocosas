<?php

class DocumentosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','consulta','archivo'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','create','update','delete'),
				'roles'=>array('admin'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','create','update','delete'),
				'roles'=>array('super'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	public function actionConsulta()
	{
		$model=new Documentos;
		if(Yii::app()->request->isAjaxRequest){
			$this->renderPartial('consulta',array(
				'model'=>$model,
			),false,true);
		}
		else{
			$this->render('consulta',array(
				'model'=>$model,
			));
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		if(Yii::app()->request->isAjaxRequest){
			$this->renderPartial('view',array(
				'model'=>$this->loadModel($id),
			),false,true);
		}
		else{
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionViewPublic()
	{
		$model=new Documentos;
		if(Yii::app()->request->isAjaxRequest){
			$this->renderPartial('viewPublic',array(
				'model'=>$model,
			),false,true);
		}
		else{
			$this->render('viewPublic',array(
				'model'=>$model,
			));
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		//$idEntrenadoresCurso = Yii::app()->request->getPost('idEntrenadoresCurso');
		$model=new Documentos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
//http://www.yiiframework.com/forum/index.php/topic/36318-storing-and-retrieving-a-file-from-postgresql-bytea-column-type/
		if(isset($_POST['Documentos']))
		{
			$model->attributes=$_POST['Documentos'];
      $model->setAttributes(
                            array(
                                  'usuariocrea'=>Yii::app()->user->id,
                                  'fechacrea'  =>new CDbExpression('NOW()'),
                                  'usuariomodifica'=>Yii::app()->user->id,
                                  'fechamodifica'  =>new CDbExpression('NOW()'),
                                 )
                            );
			$file=CUploadedFile::getInstance($model,'archivo');
      $model->documento   = $file->name;
      $model->descripcion = $file->type.' '.$file->size;
      $content=file_get_contents($file->tempName);
      
      //$path = $file->tempName;
      //$file->saveAs($path); //save the file to the $path
      //$fp   = fopen($path, 'rb');
      //$content = fread($fp, filesize($file->tempName));  //get the file content
      //fclose($fp);
      $data = pg_escape_bytea($content); 
      $model->archivo = new CDbExpression("'{$data}'");
      //print_r($model->archivo);
      //Yii::app()->end();

      //pg_escape_bytea($data); guardar pg_bytea_decode extraer
			if($model->save()){
				if(Yii::app()->request->isAjaxRequest){
					echo (1)."---".$model->id;
					Yii::app()->end();
				}
				else{
					$this->redirect(array('view','id'=>$model->id));
					/*$this->render('view',array(
			            'id'=>$model->id,
			            'model'=>$this->loadModel($model->id),
			            'idEntrenadoresCurso'=>$idEntrenadoresCurso,
			        ));
			        Yii::app()->end();*/
				}
			}
		}
		
		if(Yii::app()->request->isAjaxRequest){
			$this->renderPartial('create',array(
				'model'=>$model,
			),false,true);
		}
		else{
			$this->render('create',array(
				'model'=>$model,
			));
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Documentos']))
		{
			$model->attributes=$_POST['Documentos'];
			if($model->save()){
				if(Yii::app()->request->isAjaxRequest){
					echo (1)."---".$model->id;
					Yii::app()->end();
				}			
				else{
					$this->redirect(array('view','id'=>$model->id));
					/*$this->render('view',array(
		            	'id'=>$model->id,
		            	'model'=>$this->loadModel($model->id),
		            	'idEntrenadoresCurso'=>$model->seguiparticipantescurso->idEntrenadoresCurso,
		          	));
		          	Yii::app()->end();
		          	*/
				}
			}
		}
		
		if(Yii::app()->request->isAjaxRequest){
			$this->renderPartial('update',array(
				'model'=>$model,
			),false,true);
		}
		else{
			$this->render('update',array(
				'model'=>$model,
			));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Documentos');
		
		if(Yii::app()->request->isAjaxRequest){
			$this->renderPartial('index',array(
				'dataProvider'=>$dataProvider,
			),false,true);
		}
		else
		{
			$this->render('index',array(
				'dataProvider'=>$dataProvider,
			));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		/*if(isset($_GET['id']))
	      $idEntrenadoresCurso = $_GET['id'];
	    else
	      $idEntrenadoresCurso = Yii::app()->request->getPost('idEntrenadoresCurso');*/

		$model=new Documentos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Documentos']))
			$model->attributes=$_GET['Documentos'];

		if(Yii::app()->request->isAjaxRequest){
			$this->renderPartial('admin',array(
				'model'=>$model,
			),false,true);
		}
		else
		{
			$this->render('admin',array(
				'model'=>$model,
			));
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Documentos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='documentos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionPdf() 
  {
		//$model=new Documentos('search');
		//$model->unsetAttributes();  // clear any default values
		//$dataProvider=new CActiveDataProvider('Documentos');
		$model=Documentos::model()->findAll();
		if(isset($_GET['pdf']))
		{
			$this->layout = "//layouts/pdf";
			# mPDF
			$mPDF1 = Yii::app()->ePdf->mpdf();
			//$mPDF1->SetWatermarkImage(Yii::app()->request->baseUrl."/images/logo.jpg", 0.1, '', array(70,80));
			//$mPDF1->showWatermarkImage = true;
			
			 # render (full page)
			$mPDF1->WriteHTML($this->render('pdf',array(
				'model' => $model,
			),true));
			
			# Outputs ready PDF
			$mPDF1->Output('Documentos_'.date("Y-m-d").'.pdf',EYiiPdf::OUTPUT_TO_DOWNLOAD);
		}
  }
	
	public function actionExcel() 
  {
		
		$model=Documentos::model()->findAll();
		
		if(isset($_GET['excel']))
		{
			$content = $this->renderPartial('excel', array(
				'model' => $model,
			),false,true);
		}
		Yii::app()->request->sendFile("Documentos_".date("Y-m-d").".xls",$content);
  }

  public function actionNewreporte()
  {
    if( $_GET['rep'] == 1 ) 
    {
      //if( Yii::app()->user->checkAccess('Administrador') ) //|| Yii::app()->user->checkAccess('Cordinador')
        //$participantescurso = Documentos::model()->findAll();
      //else
        $Documentos = Documentos::model()->findAllByAttributes(array('idEntrenadoresCurso'=>$_GET['idEntrenadoresCurso']));
        //$Documentos    = Documentos::model()->findByPk($_GET['idEntrenadoresCurso']);
        //$curso              = $entrenadorcurso->curso->nombre;
    }
    else if(  $_GET['rep'] == 2  ) 
    {
      $Documentos=Documentos::model()->findAllByPk($_GET['id']);
    }
    
    if(isset($_GET['rep']))
    {
      $content = $this->renderPartial('newreporte', array(
        'Documentos' => $Documentos,
      ),true);
      Yii::app()->request->sendFile("Documentos_".date("Y_m_d").".xls",$content);
    }
  }

  public function actionImportar() 
  {
    ini_set('max_execution_time', 300); 
    $model = new Documentos;
    $file_path = Yii::app()->basePath.'/../files/usuarios.xls';
    $sheet_array = Yii::app()->yexcel->readActiveSheet($file_path);  
    $fil = 1;
    foreach( $sheet_array as $row ) {
      $col = 1;
      

        foreach( $row as $column ) {
          $valor = trim($column);
          switch ($col) {
            case 1:
              $cedula = $valor;
            break;
            case 2:
              $nombres = $valor;
            break;
            case 3:
              $apellidos = $valor;
            break;
          }
          $col++;
          if($col==8)
            break;
        }
        if( $cedula != '' )
        {
            $usuario = new Usuario;
            $usuario->Tipo = 'Estudiante';
            $usuario->nombres = $nombres;
            $usuario->apellidos = $apellidos;
            $usuario->cedula = $cedula;
            $usuario->estado = 'activo';
            $usuario->password=$model->hashPassword($nombres,$session=$model->generateSalt());
            $usuario->session=$session;
            
            if($usuario->save()){
              $asign = new AuthAsignacion;
              $asign->itemname = 'rol_estudiante';//$_POST['rolUsuario'];
              $asign->userid = $model->id;
              $asign->data = 's:0:"";';
              
              if($asign->save())
              {
                ;
              }
            }
        }
      
      if( $column=='')
        break;
      $fil++;
    }
    Yii::app()->user->setFlash('success', "Datos cargados exitosamente!");
    //Yii::app()->end();
    
    $model=new Documentos('search');
    $model->unsetAttributes();  // clear any default values
    if(isset($_GET['Documentos']))
      $model->attributes=$_GET['Documentos'];

    if(Yii::app()->request->isAjaxRequest){
      $this->renderPartial('admin',array(
        'model'=>$model,
      ),false,true);
    }
    else
    {
      $this->render('admin',array(
        'model'=>$model,
      ));
    }
  }

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionArchivo($id)
  {    
    //http://stackoverflow.com/questions/22661515/store-image-into-postgresql-bytea-field-using-yiiframework
    $model    = $this->loadModel($id);
    $contents = stream_get_contents($model->archivo);
    echo $contents;
    // Yii::app()->end();
    $dir      = Yii::app()->basePath . '/../files/';
   $name     = $model->documento;
   //$flag     = $this->bin2File($contents, $dir, $name);

    // header('Content-Description: File Transfer');
    // header('Content-Transfer-Encoding: binary');
    // header('Cache-Control: public, must-revalidate, max-age=0');
    // header('Pragma: public');
    // header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
    // header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    // header('Content-type:image/png');

    // header('Content-Type: application/force-download');
    // header('Content-Type: application/octet-stream', false);
    // header('Content-Type: application/download', false);
    // header('Content-Type: application/pdf', false);
    // header('Content-disposition: attachment; filename="'.$name.'"');
       // Yii::app()->end();
       
header ('Content-type: image/jpg');
Echo pg_unescape_bytea ($contents);
//echo pg_unescape_bytea($file);


    // echo $contents;
    //$this->assertTrue($flag);
  }

  /**
   * Creates file in original format given the binary file contents, the directory name, and the filename
   * @param string $contents the contents of the document in binary format
   * @param string $dir the directory name 
   * @param string $name the document or file name 
   * @return boolean true for success otherwise false 
   * pg_unescape_bytea() 
   */
  public function bin2File($contents, $dir, $name) 
  {
    //$contents = pg_unescape_bytea($contents); //pg_bytea_decode
    $fileName = $this->createPath($dir, $name);
    $bytes    = file_put_contents($fileName, $contents);
    return (sizeof($bytes) > 0);
  }

  /**
   * Build requested path using application root path and requested dir and file name 
   * @param string $dir the requested directory  name
   * @param string $filename the requested file name
   * @return string the full path  
   */
  public function createPath($dir, $fileName=null) 
  {
    $basePath = Yii::app()->basePath;       
    $pos      = strrpos($basePath,'/');
    $path     = '';//substr($basePath, 0, $pos + 1);
    if ($fileName != null)
      return ($path . $dir . '/' . $fileName);
    else 
      return ($path . $dir);
  }
}
