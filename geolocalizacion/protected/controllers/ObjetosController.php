<?php
Yii::import('ext.gmap.*');
class ObjetosController extends Controller
{
	

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions

				'actions'=>array('admin','create','update','delete','desactivar'),
				'users'=>array('jaco'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','create','update','delete','desactivar'),
				'users'=>array('jaco'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

public function actionDesactivar($id)
	{
		$model=Objetos::model()->findByPk($_GET['id']);
		// var_dump(expression)
		if ($model->status == true) {
		$model->status = false;
		echo "ta";
			$model->save();

		}
		else {
			echo "nota";
			$model->status = true;
			$model->save();
		}
		$this->redirect(array('/objetos'));
		// if($model===null)
			// throw new CHttpException(404,'The requested page does not exist.');
		// return $model;
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Objetos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Objetos']))
		{
			$model->attributes=$_POST['Objetos'];
			$model->imagen=CUploadedFile::getInstance($model,'imagen');
			$model->imagen2=CUploadedFile::getInstance($model,'imagen2');
			$model->imagen3=CUploadedFile::getInstance($model,'imagen3');

			if($model->save()) {
            	
            	   if($model->imagen !== null){ // only do if file is really uploaded
            	  $model->imagen->saveAs(Yii::app()->basePath .'/../../FRONTgeolocalizacion/images/objetos/' . $model->idobjeto.$model->imagen.'.jpg');
            }        
            if($model->imagen2 !== null){ // only do if file is really uploaded
            	  $model->imagen2->saveAs(Yii::app()->basePath .'/../../FRONTgeolocalizacion/images/objetos/' . $model->idobjeto.$model->imagen2.'.jpg');
            }        
            if($model->imagen3 !== null){ // only do if file is really uploaded
            	  $model->imagen3->saveAs(Yii::app()->basePath .'/../../FRONTgeolocalizacion/images/objetos/' . $model->idobjeto.$model->imagen3.'.jpg');
            }        

				$this->redirect(array('view','id'=>$model->idobjeto));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$url = Yii::app()->basePath .'/../../FRONTgeolocalizacion/images/objetos/';
		if(isset($_POST['Objetos']))
		{       
			$_POST['Objetos']['imagen'] = $model->imagen;
			$_POST['Objetos']['imagen2'] = $model->imagen2;
			$_POST['Objetos']['imagen3'] = $model->imagen3;
			$model->attributes=$_POST['Objetos'];
            $uploadedFile=CUploadedFile::getInstance($model,'imagen');
            $uploadedFile2=CUploadedFile::getInstance($model,'imagen2');
            $uploadedFile3=CUploadedFile::getInstance($model,'imagen3');
  		  if($model->save())
            {  
                if(!empty($uploadedFile))  // check if uploaded file is set or not
                {
                	$goto= $url.$model->idobjeto.$uploadedFile->name;
                    $uploadedFile->saveAs($goto);
                    $model->imagen = $uploadedFile->name;
                    $model->save();
                }

                if(!empty($uploadedFile2))  // check if uploaded file is set or not
                {
                	$goto= $url.$model->idobjeto.$uploadedFile2->name;
                    $uploadedFile2->saveAs($goto);
                    $model->imagen2 = $uploadedFile2->name;
                    $model->save();
                }

                if(!empty($uploadedFile3))  // check if uploaded file is set or not
                {
                	$goto= $url.$model->idobjeto.$uploadedFile3->name;
                    $uploadedFile3->saveAs($goto);
                    $model->imagen3 = $uploadedFile3->name;
                    $model->save();
                }


				$this->redirect(array('view','id'=>$model->idobjeto));
            }









		}

		$this->render('update',array(
			'model'=>$model,
		));
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
	
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Objetos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Objetos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Objetos']))
			$model->attributes=$_GET['Objetos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Objetos the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Objetos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Objetos $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='objetos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}
