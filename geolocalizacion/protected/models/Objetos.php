<?php

/**
 * This is the model class for table "objetos".
 *
 * The followings are the available columns in table 'objetos':
 * @property integer $idobjeto
 * @property string $nombre
 * @property string $descripcion
 * @property string $direccion
 * @property string $latitud
 * @property string $longitud
 *
 * The followings are the available model relations:
 * @property Categoriaobjeto[] $categoriaobjetos
 * @property Atributoobjeto[] $atributoobjetos
 * @property Objetoetiqueta[] $objetoetiquetas
 * @property Visitas[] $visitases
 * @property Comentarios[] $comentarioses
 * @property Calificaciones[] $calificaciones
 */
class Objetos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'objetos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, descripcion, direccion, latitud, longitud, zoom', 'required','message'=>'por  favor ingresar: {attribute}'), 
			array('nombre', 'length', 'max'=>80),
			array('descripcion', 'length', 'max'=>300),
			array('direccion, latitud, longitud', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idobjeto, nombre, descripcion, direccion, latitud, longitud, zoom', 'safe', 'on'=>'search'),
			// array('imagen, imagen3, imagen2', 'length', 'max'=>255, 'on'=>'insert,update'),
			// array('imagen, imagen3, imagen2', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'), // this will allow empty field when page is update (remember here i create scenario update)


		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categoriaobjetos' => array(self::HAS_MANY, 'Categoriaobjeto', 'idobjeto'),
			'atributoobjetos' => array(self::HAS_MANY, 'Atributoobjeto', 'idobjeto'),
			'objetoetiquetas' => array(self::HAS_MANY, 'Objetoetiqueta', 'idobjeto'),
			'visitases' => array(self::HAS_MANY, 'Visitas', 'idobjeto'),
			'comentarioses' => array(self::HAS_MANY, 'Comentarios', 'idobjeto'),
			'calificaciones' => array(self::HAS_MANY, 'Calificaciones', 'idobjeto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idobjeto' => 'Idobjeto',
			'nombre' => 'Nombre',
			'descripcion' => 'descripcion',
			'direccion' => 'Direccion',
			'latitud' => 'Latitud',
			'longitud' => 'Longitud',
			'zoom' => 'Zoom',
			

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idobjeto',$this->idobjeto);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('latitud',$this->latitud,true);
		$criteria->compare('longitud',$this->longitud,true);
		$criteria->compare('zoom',$this->zoom,true);



		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Objetos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
