<?php

/**
 * This is the model class for table "atributos".
 *
 * The followings are the available columns in table 'atributos':
 * @property integer $idatributo
 * @property string $nombre
 * @property string $tipo
 * @property string $caracter
 *
 * The followings are the available model relations:
 * @property Atributoobjeto[] $atributoobjetos
 */
class Atributos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'atributos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, tipo, caracter', 'required','message'=>'por  favor ingresar: {attribute}'), 
			array('nombre', 'length', 'max'=>80),
			array('tipo, caracter', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idatributo, nombre, tipo, caracter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'atributoobjetos' => array(self::HAS_MANY, 'Atributoobjeto', 'idatributo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idatributo' => 'Idatributo',
			'nombre' => 'Nombre',
			'tipo' => 'Tipo',
			'caracter' => 'Caracter',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idatributo',$this->idatributo);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('caracter',$this->caracter,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Atributos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
