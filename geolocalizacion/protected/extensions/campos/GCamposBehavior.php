<?php
class GCamposBehavior extends CActiveRecordBehavior {
	
	public $usuariocrea = 'usuariocrea';
	public $campoFechaCreacion = 'creado';
	public $usuariomodifica = 'usuariomodifica';
	public $campoFechaActualizacion = 'actualizado';
	public $campoIp = 'ip';
	public $setActualizacionAlCrear = true;
	public function beforeSave($event) {
            if ($this->getOwner()->getIsNewRecord()) {
                if($this->getOwner()->getTableSchema()->getColumn($this->campoFechaCreacion)!==null)    
                    $this->getOwner()->{$this->campoFechaCreacion} = date('Y-m-d H:i:s');
            }
            if ((!$this->getOwner()->getIsNewRecord() || $this->setActualizacionAlCrear) && ($this->getOwner()->getTableSchema()->getColumn($this->campoFechaActualizacion)!==null)) {
                    $this->getOwner()->{$this->campoFechaActualizacion} = date('Y-m-d H:i:s');
            }
            if ($this->getOwner()->getIsNewRecord()){
            if (($this->getOwner()->getTableSchema()->getColumn($this->usuariocrea)!==null)) 
                $this->getOwner()->{$this->usuariocrea} = Yii::app()->user->name;
			}
			 if (($this->getOwner()->getIsNewRecord() || $this->setActualizacionAlCrear) && ($this->getOwner()->getTableSchema()->getColumn($this->usuariomodifica)!==null)){
                $this->getOwner()->{$this->usuariomodifica} = Yii::app()->user->name;
			 }
            if ($this->getOwner()->getTableSchema()->getColumn($this->campoIp)!==null) 
                $this->getOwner()->{$this->campoIp} = Yii::app()->request->userHostAddress;
	}
}