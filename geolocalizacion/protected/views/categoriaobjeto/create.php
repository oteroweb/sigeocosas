<?php
/* @var $this CategoriaobjetoController */
/* @var $model Categoriaobjeto */

$this->breadcrumbs=array(
	'Categoriaobjetos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Categoriaobjeto', 'url'=>array('index')),
	array('label'=>'Manage Categoriaobjeto', 'url'=>array('admin')),
);
?>

<h1>Create Categoriaobjeto</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>