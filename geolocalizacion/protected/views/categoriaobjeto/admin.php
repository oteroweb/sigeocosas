<?php
/* @var $this CategoriaobjetoController */
/* @var $model Categoriaobjeto */

$this->breadcrumbs=array(
	'Categoriaobjetos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Categoriaobjeto', 'url'=>array('index')),
	array('label'=>'Create Categoriaobjeto', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#categoriaobjeto-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Categoriaobjetos</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'categoriaobjeto-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'idcateobje',
		array('name'=>'idcategoria','value'=>'$data->idcategoria0->nombre'),
		
		array('name'=>'idobjeto','value'=>'$data->idobjeto0->nombre'),
		
		
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
