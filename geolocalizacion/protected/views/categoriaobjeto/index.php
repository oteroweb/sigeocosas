<?php
/* @var $this CategoriaobjetoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Categoriaobjetos',
);

$this->menu=array(
	array('label'=>'Create Categoriaobjeto', 'url'=>array('create')),
	array('label'=>'Manage Categoriaobjeto', 'url'=>array('admin')),
);
?>

<h1>Lista de Categoriaobjetos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
		

