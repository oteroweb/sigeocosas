<?php Yii::import('ext.fontawesome.*'); ?>


<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">



	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

	
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-2.1.1.min.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD0yeJJ5y46rJnJanMSvZvjRYVZLLMkfdE"></script>
	
	<link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl; ?>/css/jquery-gmaps-latlon-picker.css"/>
</head>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-gmaps-latlon-picker.js"></script>

	<?php
	echo yii::app()->bootstrap->registerAllCss();
	echo yii::app()->bootstrap->registerCoreScripts();
	?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				
				array('label'=>'Categorias', 'url'=>array('/categorias/index')),
				array('label'=>'ObjetoCategoria', 'url'=>array('/categoriaobjeto/index')),
				array('label'=>'Objetos', 'url'=>array('/objetos/index')),
				array('label'=>'ObjetosAtributos', 'url'=>array('/atributoobjeto/index')),
				array('label'=>'Atributos', 'url'=>array('/atributos/index')),
				array('label'=>'ObjetoEtiqueta', 'url'=>array('/objetoetiqueta/index')),
				array('label'=>'Etiquetas', 'url'=>array('/etiquetas/index')),
				array('label'=>'Visitas', 'url'=>array('/visitas/index')),
				array('label'=>'Comentarios', 'url'=>array('/comentarios/index')),
				array('label'=>'Calificaciones', 'url'=>array('/calificaciones/index')),
				array('label'=>'Usuarios', 'url'=>array ('/usuarios/index')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
		


	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>
	

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
