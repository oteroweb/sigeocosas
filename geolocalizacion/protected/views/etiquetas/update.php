<?php
/* @var $this EtiquetasController */
/* @var $model Etiquetas */

$this->breadcrumbs=array(
	'Etiquetases'=>array('index'),
	$model->idetiqueta=>array('view','id'=>$model->idetiqueta),
	'Update',
);

$this->menu=array(
	array('label'=>'List Etiquetas', 'url'=>array('index')),
	array('label'=>'Create Etiquetas', 'url'=>array('create')),
	array('label'=>'View Etiquetas', 'url'=>array('view', 'id'=>$model->idetiqueta)),
	array('label'=>'Manage Etiquetas', 'url'=>array('admin')),
);
?>

<h1>Update Etiquetas <?php echo $model->idetiqueta; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>