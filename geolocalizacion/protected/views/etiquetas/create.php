<?php
/* @var $this EtiquetasController */
/* @var $model Etiquetas */

$this->breadcrumbs=array(
	'Etiquetases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Etiquetas', 'url'=>array('index')),
	array('label'=>'Manage Etiquetas', 'url'=>array('admin')),
);
?>

<h1>Create Etiquetas</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>