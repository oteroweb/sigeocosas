<?php
$this->breadcrumbs=array(
	//'Documentoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	//array('label'=>Yii::t('app','List').' Documentos','url'=>array('index')),
	array('label'=>Yii::t('app','Create').' Documentos','url'=>array('create')),
	//array('label'=>Yii::t('app','Create').' ','url'=>'javascript:void(0);', 'linkOptions'=>array('submit'=>array('create'),'params'=>array('idEntrenadoresCurso'=>$idEntrenadoresCurso))),
	array('label'=>Yii::t('app','View').' Documentos','url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('app','Manage').' Documentos','url'=>array('admin')),
	//array('label'=>Yii::t('app','Manage').' ','url'=>'javascript:void(0);', 'linkOptions'=>array('submit'=>array('admin'),'params'=>array('idEntrenadoresCurso'=>$idEntrenadoresCurso))),
);
?>
<div class="page-header"><!-- , 'params'=>array('idEntrenadoresCurso'=>$idEntrenadoresCurso) -->
<h4><span class="label label-danger">Actualizar Documentos <?php echo $model->id; ?></span> - <?php echo CHtml::link(Yii::t('app','Manage').' Documentos','javascript:void(0);', array('submit'=>array('admin'))); ?></h4>
</div>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>