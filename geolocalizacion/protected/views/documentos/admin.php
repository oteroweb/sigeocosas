<?php
$this->breadcrumbs=array(
	//'Documentoses'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>Yii::t('app','List').' Documentos','url'=>array('index')),
	array('label'=>Yii::t('app','Create').' Documentos','url'=>array('create')),
  //array('label'=>Yii::t('app','Create').' ','url'=>'javascript:void(0);', 'linkOptions'=>array('submit'=>array('create'),'params'=>array('idEntrenador'=>$idEntrenador))),
	array('label'=>Yii::t('app','Excel').' Documentos', 'url'=>array('excel','excel'=>1)),
	array('label'=>Yii::t('app','Pdf').' Documentos', 'url'=>array('pdf','pdf'=>1)),
  //array('label'=>Yii::t('app','Volver').' ', 'url'=>array('/entrenadores/admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('documentos-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-header">
<h4><span class="label label-danger">Administrar Documentoses</span></h4>
</div>

<p>
También puede escribir un operador de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al comienzo de cada uno de sus valores de búsqueda para especificar cómo se debe hacer la comparación.
</p>

<h4>
<?php echo CHtml::link('<i class="fa fa-search"></i> Búsqueda Avanzada','#',array('class'=>'search-button btn btn-danger')); ?><?php echo CHtml::link('<i class="fa fa-plus"></i> Crear Documentos', 'create',array('class'=>'btn btn-danger', 'style'=>'float:right;')); ?></h4>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'documentos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'id',
			'header'=>'ID',
			'value'=>'$data->id',
			'filter'=>false,//Cursos::getListCursos(),
			//'cssClassExpression'=>'($data->id->estado==activo)?rowActiva:rowInactiva',
		),
		array(
			'name'=>'documento',
			'header'=>'DOCUMENTO',
			'value'=>'$data->documento',
			'filter'=>false,//Cursos::getListCursos(),
			//'cssClassExpression'=>'($data->documento->estado==activo)?rowActiva:rowInactiva',
		),
		array(
			'name'=>'descripcion',
			'header'=>'DESCRIPCION',
			'value'=>'$data->descripcion',
			'filter'=>false,//Cursos::getListCursos(),
			//'cssClassExpression'=>'($data->descripcion->estado==activo)?rowActiva:rowInactiva',
		),
		array(
			'name'=>'idserie',
			'header'=>'IDSERIE',
			'value'=>'$data->idserie',
			'filter'=>false,//Cursos::getListCursos(),
			//'cssClassExpression'=>'($data->idserie->estado==activo)?rowActiva:rowInactiva',
		),
		array(
			'name'=>'idsubserie',
			'header'=>'IDSUBSERIE',
			'value'=>'$data->idsubserie',
			'filter'=>false,//Cursos::getListCursos(),
			//'cssClassExpression'=>'($data->idsubserie->estado==activo)?rowActiva:rowInactiva',
		),
		/*
		array(
			'name'=>'archivo',
			'header'=>'ARCHIVO',
			'value'=>'$data->archivo',
			'filter'=>false,//Cursos::getListCursos(),
			//'cssClassExpression'=>'($data->archivo->estado==activo)?rowActiva:rowInactiva',
		),
		
		array(
			'name'=>'usuariocrea',
			'header'=>'USUARIOCREA',
			'value'=>'$data->usuariocrea',
			'filter'=>false,//Cursos::getListCursos(),
			//'cssClassExpression'=>'($data->usuariocrea->estado==activo)?rowActiva:rowInactiva',
		),
		array(
			'name'=>'fechacrea',
			'header'=>'FECHACREA',
			'value'=>'$data->fechacrea',
			'filter'=>false,//Cursos::getListCursos(),
			//'cssClassExpression'=>'($data->fechacrea->estado==activo)?rowActiva:rowInactiva',
		),
		array(
			'name'=>'usuariomodifica',
			'header'=>'USUARIOMODIFICA',
			'value'=>'$data->usuariomodifica',
			'filter'=>false,//Cursos::getListCursos(),
			//'cssClassExpression'=>'($data->usuariomodifica->estado==activo)?rowActiva:rowInactiva',
		),
		array(
			'name'=>'fechamodifica',
			'header'=>'FECHAMODIFICA',
			'value'=>'$data->fechamodifica',
			'filter'=>false,//Cursos::getListCursos(),
			//'cssClassExpression'=>'($data->fechamodifica->estado==activo)?rowActiva:rowInactiva',
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('style' => 'width:100px;'),
      'template'=>'{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}&nbsp;&nbsp;{archivos}',
      'buttons'=>array
      (
        'archivos' => array
        (
          'label'=>'<i class="fa fa-file-o"></i>',
          'options'=>array('title'=>'Archivo', 'id'=>'archivo_link'),
          'url'=>'Yii::app()->controller->createUrl("archivo",array("id"=>$data->id))',
          'visible'=>'Yii::app()->user->checkAccess("Administrador")',
        ),
      ),
      //'cssClassExpression'=>'($data->usuario->estado=="activo")?"rowActiva":"rowInactiva"',
		),
	),
	'pager'=>array(
		'htmlOptions'=>array('class'=>'pagination pagination-sm'),
	),
)); ?>
