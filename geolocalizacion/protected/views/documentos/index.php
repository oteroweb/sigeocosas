<?php
$this->breadcrumbs=array(
	'Documentoses',
);

$this->menu=array(
	array('label'=>Yii::t('app','Create').' Documentos','url'=>array('create')),
	array('label'=>Yii::t('app','Manage').' Documentos','url'=>array('admin')),
);
?>

<h1>Documentoses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	//'viewData'=>array('idDependencia'=>$idDependencia),
	'pager'=>array(
		'htmlOptions'=>array('class'=>'pagination pagination-sm'),
	),
)); ?>
