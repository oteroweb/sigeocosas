<?php
$this->breadcrumbs=array(
	'Documentoses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List').' Documentos','url'=>array('index')),
	array('label'=>Yii::t('app','Create').' Documentos','url'=>array('create')),
	array('label'=>Yii::t('app','Update').' Documentos','url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('app','Delete').' Documentos','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage').' Documentos','url'=>array('admin')),
);
?>
<div class="page-header">
<h4><span class="label label-danger">Mostrar Documentos #<?php echo $model->id; ?></span> - <?php echo CHtml::link(Yii::t('app','Manage').' Documentos','javascript:void(0);', array('submit'=>array('admin'))); ?></h4>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'documento',
		'descripcion',
		'idserie',
		'idsubserie',
		//'archivo',
		'usuariocrea',
		'fechacrea',
		'usuariomodifica',
		'fechamodifica',
	),
)); ?>
