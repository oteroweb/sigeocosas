<?php
$this->breadcrumbs=array(
	//'Documentoses'=>array('index'),
	'Buscar',
);

?>
<div class="page-header">
	<h4><span class="label label-danger">Buscar Documentos</span></h4>
</div>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'documentos-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="text-danger">Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'documento',array('class'=>'form-control','maxlength'=>255)); ?>
	<?php echo $form->textAreaRow($model,'descripcion',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
	<?php echo $form->textFieldRow($model,'idserie',array('class'=>'form-control')); ?>
	<?php echo $form->textFieldRow($model,'idsubserie',array('class'=>'form-control')); ?>

	<!-- <p class="bg-danger">Nueva sección </p>-->

	<div class="form-group">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'danger',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
<script>
  jQuery(function() {

    jQuery('.date').datetimepicker({
      format: 'YYYY-MM-DD'
    });

    jQuery('.chosen-select').chosen({search_contains: true });
    jQuery('.chosen-select-deselect').chosen({ allow_single_deselect: true, search_contains: true, width: "100%" });
  });
</script>