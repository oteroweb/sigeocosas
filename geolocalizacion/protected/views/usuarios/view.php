<?php
/* @var $this UsuariosController */
/* @var $model Usuarios */

$this->breadcrumbs=array(
	'Usuarioses'=>array('index'),
	$model->idusuario,
);

$this->menu=array(
	array('label'=>'List Usuarios', 'url'=>array('index')),
	array('label'=>'Create Usuarios', 'url'=>array('create')),
	array('label'=>'Update Usuarios', 'url'=>array('update', 'id'=>$model->idusuario)),
	array('label'=>'Delete Usuarios', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idusuario),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Usuarios', 'url'=>array('admin')),
);
?>

<h1>View Usuarios #<?php echo $model->idusuario; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idusuario',
		'nombre',
		'password',
		'email',
		'username',
	),
)); ?>

<div class="row-fluid">
	<div class="span6">

	<div class="span6">
<ul class="nav nav-tabs nav-stacked">
<?php foreach (yii::app()->authManager->getAuthItems() as $data):?>
<?php $enabled=yii::app()->authManager->checkAccess($data->name,$model->idusuario)?> 
<li>
	<h4><?php echo $data->name?>
		<small>
			<?php if($data->type==0) echo "Role";?>
			<?php if($data->type==1) echo "Tarea";?>
			<?php if($data->type==2) echo "Operación";?>
		</small>
		<?php echo CHtml::link($enabled?"off":"on",array("usuarios/assign","id"=>$model->idusuario,"item"=>$data->name),
		array("class"=>$enabled?"btn btn-primary":"btn"));?>
	</h4>
	<p>    <?php echo $data->description?></p>
		    <hr>
</li>
	<?php endforeach;?>
</ul>
	</div>
	</div>
</div>



 ingresar con:
<?php $this->widget('application.modules.hybridauth.widgets.renderProviders'); ?>




	





