<?php
/* @var $this CategoriasController */
/* @var $data Categorias */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idcategoria')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idcategoria), array('view', 'id'=>$data->idcategoria)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('Objetos')); ?>:</b>
	<?php 
	$categoriaobjeto=Objetos::model()->findAllBySql('select objetos.nombre 
from categoriaobjeto INNER JOIN objetos on categoriaobjeto.idobjeto = objetos.idobjeto where categoriaobjeto.idcategoria ='.$data->idcategoria);
foreach ($categoriaobjeto as $key => $value) {
	echo $value['nombre'].'<br>';
}

	?>
	<br />




</div>