<?php
/* @var $this AtributoobjetoController */
/* @var $model Atributoobjeto */

$this->breadcrumbs=array(
	'Atributoobjetos'=>array('index'),
	$model->idatriobje=>array('view','id'=>$model->idatriobje),
	'Update',
);

$this->menu=array(
	array('label'=>'List Atributoobjeto', 'url'=>array('index')),
	array('label'=>'Create Atributoobjeto', 'url'=>array('create')),
	array('label'=>'View Atributoobjeto', 'url'=>array('view', 'id'=>$model->idatriobje)),
	array('label'=>'Manage Atributoobjeto', 'url'=>array('admin')),
);
?>

<h1>Update Atributoobjeto <?php echo $model->idatriobje; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>