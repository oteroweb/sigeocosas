<?php
/* @var $this AtributoobjetoController */
/* @var $model Atributoobjeto */

$this->breadcrumbs=array(
	'Atributoobjetos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Atributoobjeto', 'url'=>array('index')),
	array('label'=>'Manage Atributoobjeto', 'url'=>array('admin')),
);
?>

<h1>Create Atributoobjeto</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>