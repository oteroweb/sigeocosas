<?php
/* @var $this ObjetoetiquetaController */
/* @var $model Objetoetiqueta */

$this->breadcrumbs=array(
	'Objetoetiquetas'=>array('index'),
	$model->idobjeetiq,
);

$this->menu=array(
	array('label'=>'List Objetoetiqueta', 'url'=>array('index')),
	array('label'=>'Create Objetoetiqueta', 'url'=>array('create')),
	array('label'=>'Update Objetoetiqueta', 'url'=>array('update', 'id'=>$model->idobjeetiq)),
	array('label'=>'Delete Objetoetiqueta', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idobjeetiq),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Objetoetiqueta', 'url'=>array('admin')),
);
?>

<h1>View Objetoetiqueta #<?php echo $model->idobjeetiq; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idobjeetiq',
		array(
   'name'=>'idetiqueta',
   'value'=>$model->idetiqueta0->nombre,
  ),
  array(
   'name'=>'idobjeto',
   'value'=>$model->idobjeto0->nombre,
  ),
	),
)); ?>
