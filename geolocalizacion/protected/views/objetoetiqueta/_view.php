<?php
/* @var $this ObjetoetiquetaController */
/* @var $data Objetoetiqueta */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idobjeetiq')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idobjeetiq), array('view', 'id'=>$data->idobjeetiq)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idetiqueta')); ?>:</b>
	<?php echo CHtml::encode($data->idetiqueta0->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idobjeto')); ?>:</b>
	<?php echo CHtml::encode($data->idobjeto0->nombre); ?>
	<br />


</div>