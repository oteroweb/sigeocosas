<?php
/* @var $this ObjetoetiquetaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Objetoetiquetas',
);

$this->menu=array(
	array('label'=>'Create Objetoetiqueta', 'url'=>array('create')),
	array('label'=>'Manage Objetoetiqueta', 'url'=>array('admin')),
);
?>

<h1>Lista de Objetoetiquetas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
