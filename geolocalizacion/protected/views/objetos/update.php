<?php
/* @var $this ObjetosController */
/* @var $model Objetos */

$this->breadcrumbs=array(
	'Objetoses'=>array('index'),
	$model->idobjeto=>array('view','id'=>$model->idobjeto),
	'Update',
);

$this->menu=array(
	array('label'=>'List Objetos', 'url'=>array('index')),
	array('label'=>'Create Objetos', 'url'=>array('create')),
	array('label'=>'View Objetos', 'url'=>array('view', 'id'=>$model->idobjeto)),
	array('label'=>'Manage Objetos', 'url'=>array('admin')),
);
?>

<h1>Update Objetos <?php echo $model->idobjeto; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>