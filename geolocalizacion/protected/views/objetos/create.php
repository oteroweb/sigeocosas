<?php
/* @var $this ObjetosController */
/* @var $model Objetos */

$this->breadcrumbs=array(
	'Objetoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Objetos', 'url'=>array('index')),
	array('label'=>'Manage Objetos', 'url'=>array('admin')),
);
?>

<h1>Create Objetos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>