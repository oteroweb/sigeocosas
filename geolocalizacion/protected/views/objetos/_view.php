<?php
/* @var $this ObjetosController */
/* @var $data Objetos */
?>

<div class="view">


	<b><?php echo CHtml::encode($data->getAttributeLabel('idobjeto')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idobjeto), array('view', 'id'=>$data->idobjeto)); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('latitud')); ?>:</b>
	<?php echo CHtml::encode($data->latitud); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('longitud')); ?>:</b>
	<?php echo CHtml::encode($data->longitud); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zoom')); ?>:</b>
	<?php echo CHtml::encode($data->zoom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagen')); ?>:</b>
	<?php echo CHtml::encode($data->imagen); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('imagen2')); ?>:</b>
	<?php echo CHtml::encode($data->imagen2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagen3')); ?>:</b>
	<?php echo CHtml::encode($data->imagen3); ?>
	<br />
	
	
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('Categorias')); ?>:</b>
	<?php 
		$categoriaobjetos=Categorias::model()->findAllBySql('select categorias.nombre from categoriaobjeto INNER JOIN categorias on categoriaobjeto.idcategoria = categorias.idcategoria where categoriaobjeto.idobjeto ='.$data->idobjeto);
foreach ($categoriaobjetos as $key => $value) {
	echo $value['nombre'].'<br>';
}

	?>
	<br />

<?php
if ($data->status ==0) {
 echo CHtml::link('activar objeto', array('objetos/desactivar', 'id'=>$data->idobjeto));
} else {
 echo CHtml::link('Desactivar objeto', array('objetos/desactivar', 'id'=>$data->idobjeto));
}

  	?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Atributos')); ?>:</b>
	<?php 
		$atributoobjetos=Atributos::model()->findAllBySql('select atributos.nombre 
from atributoobjeto INNER JOIN atributos on atributoobjeto.idatributo = atributos.idatributo where atributoobjeto.idobjeto ='.$data->idobjeto);
foreach ($atributoobjetos as $key => $value) {
	echo $value['nombre'].'<br>';
}

	?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('Etiquetas')); ?>:</b>
	<?php 
		$objetoetiquetas=Etiquetas::model()->findAllBySql('select etiquetas.nombre 
from objetoetiqueta INNER JOIN etiquetas on objetoetiqueta.idetiqueta = etiquetas.idetiqueta where objetoetiqueta.idobjeto ='.$data->idobjeto);
foreach ($objetoetiquetas as $key => $value) {
	echo $value['nombre'].'<br>';
}

	?>
	<br />


	
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('visitases')); ?>:</b>
	<?php 
	foreach ($data->visitases as $exp){
		echo $exp->idvisitas.'<br>';
	}
	?>
	<br />
	
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('comentarioses')); ?>:</b>
	<?php 
	foreach ($data->comentarioses as $exp){
		echo $exp->comentario.'<br>';
	}
	?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('calificaciones')); ?>:</b>
	<?php 
	foreach ($data->calificaciones as $exp){
		echo $exp->calificacion.'<br>';
	}
	?>
	<br />


</div>


