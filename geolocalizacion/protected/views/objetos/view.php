<?php
/* @var $this ObjetosController */
/* @var $model Objetos */

$this->breadcrumbs=array(
	'Objetoses'=>array('index'),
	$model->idobjeto,
);

$this->menu=array(
	array('label'=>'List Objetos', 'url'=>array('index')),
	array('label'=>'Create Objetos', 'url'=>array('create')),
	array('label'=>'Update Objetos', 'url'=>array('update', 'id'=>$model->idobjeto)),
	array('label'=>'Delete Objetos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idobjeto),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Objetos', 'url'=>array('admin')),
);
?>

<h1>View Objetos #<?php echo $model->idobjeto; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idobjeto',
		'nombre',
		'descripcion',
		'direccion',
		'latitud',
		'longitud',
		'zoom','imagen','imagen2','imagen3',

	),
)); ?>
