<?php
/* @var $this ObjetosController */
/* @var $model Objetos */
/* @var $form CActiveForm */
?>

<div class="form">
	<div class="gllpLatlonPicker">
    <fieldset class="gllpLatlonPicker">
	<br/> <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'objetos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

<div class="control-group">

    <?php echo $form->fileField($model,'imagen', array('class'=>'input-file')); ?>
    <?php echo $form->error($model,'imagen'); ?>
</div>

<div class="control-group">
    <?php echo $form->fileField($model,'imagen2', array('class'=>'input-file')); ?>
    <?php echo $form->error($model,'imagen2'); ?>
</div>

<div class="control-group">
    <?php echo $form->fileField($model,'imagen3', array('class'=>'input-file')); ?>
    <?php echo $form->error($model,'imagen3'); ?>
</div>










	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>



<input type="text" class="gllpSearchField" />

<input type="button" class="gllpSearchButton" value="search">
<!-- <div class="gllpMap">Google Maps</div> -->


	<div class="gllpMap">
	</div>
		<?php echo $form->labelEx($model,'latitud'); ?>
		<?php echo $form->textField($model,'latitud',array('size'=>40,'maxlength'=>40, 'value'=> $model->latitud, 'class'=>'gllpLatitude',"value"=>"5.452117138540118")); ?>
		<?php echo $form->error($model,'latitud'); ?>
	</br>

		<?php echo $form->labelEx($model,'longitud'); ?>
		<?php echo $form->textField($model,'longitud',array('size'=>40,'maxlength'=>40,'value'=> $model->longitud, 'class'=>'gllpLongitude',"value"=>"-74.66592474257811")); ?>
		<?php echo $form->error($model,'longitud'); ?>
</br>
		<?php echo $form->labelEx($model,'zoom'); ?>
		<?php echo $form->textField($model,'zoom',array('size'=>40,'maxlength'=>40,'value'=> $model->zoom, 'class'=>'gllpZoom',"value"=>"14")); ?>
		<?php echo $form->error($model,'zoom'); ?>


	<div class="row">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>


	

<?php $this->endWidget(); ?>
</fieldset>
  </div>


</div><!-- form -->


