<?php

$this->pageTitle = 'recuperar password';
$this->breadcrumbs = array('recuperar password');
echo $msg;
?>

<div class="form">
    <?php $form = $this->beginWidget ('CActiveForm',
 array(
     'method' => 'post',
     'action' => yii::app()->createUrl('site/recuperarpassword'),
     'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
 )); 
?>
    

     <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username'); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>
    
     <div class="row">
        <?php
                echo $form->labelEX($model,'captcha');
                $this->widget('CCaptcha', array('buttonLabel'=>'Actulizar codigo'));
                echo $form->textField($model, 'captcha');
                ?>
          <div class="text-info">
        por favor introducir el codigo de la imagen jajaj
        </div>
         <?php echo $form->error($model,'captcha', array ('class' => 'text-error'));?>
    </div>
    
    <div class="row ">
        <?php echo CHtml::submitButton('recuperar password', array('class' => 'btn btn-primary')); ?>
    </div>
   
    <?php $this->endWidget();?>
</div>

