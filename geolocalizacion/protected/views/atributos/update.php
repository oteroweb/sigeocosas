<?php
/* @var $this AtributosController */
/* @var $model Atributos */

$this->breadcrumbs=array(
	'Atributoses'=>array('index'),
	$model->idatributo=>array('view','id'=>$model->idatributo),
	'Update',
);

$this->menu=array(
	array('label'=>'List Atributos', 'url'=>array('index')),
	array('label'=>'Create Atributos', 'url'=>array('create')),
	array('label'=>'View Atributos', 'url'=>array('view', 'id'=>$model->idatributo)),
	array('label'=>'Manage Atributos', 'url'=>array('admin')),
);
?>

<h1>Update Atributos <?php echo $model->idatributo; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>