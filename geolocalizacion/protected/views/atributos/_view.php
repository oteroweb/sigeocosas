<?php
/* @var $this AtributosController */
/* @var $data Atributos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idatributo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idatributo), array('view', 'id'=>$data->idatributo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo')); ?>:</b>
	<?php echo CHtml::encode($data->tipo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('caracter')); ?>:</b>
	<?php echo CHtml::encode($data->caracter); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('Objetos')); ?>:</b>
	<?php 
		$atributoobjeto=Objetos::model()->findAllBySql('select objetos.nombre 
from atributoobjeto INNER JOIN objetos on atributoobjeto.idobjeto = objetos.idobjeto where atributoobjeto.idatributo ='.$data->idatributo);
foreach ($atributoobjeto as $key => $value) {
	echo $value['nombre'].'<br>';
}

	?>
	<br />


</div>