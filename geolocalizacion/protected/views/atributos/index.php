<?php
/* @var $this AtributosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Atributoses',
);

$this->menu=array(
	array('label'=>'Create Atributos', 'url'=>array('create')),
	array('label'=>'Manage Atributos', 'url'=>array('admin')),
);
?>

<h1>Lista de Atributos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
