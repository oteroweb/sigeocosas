<?php
/* @var $this AtributosController */
/* @var $model Atributos */

$this->breadcrumbs=array(
	'Atributoses'=>array('index'),
	$model->idatributo,
);

$this->menu=array(
	array('label'=>'List Atributos', 'url'=>array('index')),
	array('label'=>'Create Atributos', 'url'=>array('create')),
	array('label'=>'Update Atributos', 'url'=>array('update', 'id'=>$model->idatributo)),
	array('label'=>'Delete Atributos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idatributo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Atributos', 'url'=>array('admin')),
);
?>

<h1>View Atributos #<?php echo $model->idatributo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idatributo',
		'nombre',
		'tipo',
		'caracter',
	),
)); ?>
