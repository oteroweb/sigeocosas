<?php

/**
 * This is the model class for table "visitas".
 *
 * The followings are the available columns in table 'visitas':
 * @property integer $idvisitas
 * @property integer $idobjeto
 * @property integer $idusuario
 *
 * The followings are the available model relations:
 * @property Objetos $idobjeto0
 * @property Usuarios $idusuario0
 */
class Visitas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'visitas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idobjeto, idusuario', 'numerical', 'integerOnly'=>true),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idvisitas, idobjeto, idusuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idobjeto0' => array(self::BELONGS_TO, 'Objetos', 'idobjeto'),
			'idusuario0' => array(self::BELONGS_TO, 'Usuarios', 'idusuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idvisitas' => 'Idvisitas',  /** * cambiarle el nombre al enunciado  */
			'idobjeto' => 'Idobjeto',
			'idusuario' => 'Idusuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idvisitas',$this->idvisitas);
		$criteria->compare('idobjeto',$this->idobjeto);
		$criteria->compare('idusuario',$this->idusuario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave() 
    {
        if($this->isNewRecord)
        {           
            $this->idobjeto= $_GET['id'];
            // $this->idusuario = Yii::app()->user->getId();
        }else{
             // $this->modifieddate = new CDbExpression('NOW()');
        }
        return parent::beforeSave();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Visitas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
