<?php

/**
 * This is the model class for table "usuarios".
 *
 * The followings are the available columns in table 'usuarios':
 * @property integer $idusuario
 * @property string $nombre
 * @property string $password
 * @property string $email
 * @property string $username
 *
 * The followings are the available model relations:
 * @property Visitas[] $visitases
 * @property Calificaciones[] $calificaciones
 * @property Comentarios[] $comentarioses
 */
class Usuarios extends CActiveRecord
{ 
	    public $verifyCode;

		

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, password, email, username', 'required'),
			array('nombre', 'length', 'max'=>80),
			array('password, username', 'length', 'max'=>255),
			array('email', 'length', 'max'=>100),
			   array('verifyCode','captcha',
        'allowEmpty'=>!CCaptcha::checkRequirements()),
    
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idusuario, nombre, password, email, username', 'safe', 'on'=>'search'),
            			// array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()), 

		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'visitases' => array(self::HAS_MANY, 'Visitas', 'idusuario'),
			'comentarioses' => array(self::HAS_MANY, 'Comentarios', 'idusuario'),
			'calificaciones' => array(self::HAS_MANY, 'Calificaciones', 'idusuario'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idusuario' => 'Idusuario',
			'nombre' => 'Nombre',
			'password' => 'password',
			'email' => 'Email',
			'session' => 'Session',
			'username' => 'Username',
            'verifyCode'=>'por favor introuzca el codigo de verificacion',

		);
	}
	
	public function validatePassword($password)
	{
		return $this->hashPassword($password,$this->session)===$this->password;
	}
	/**
	 * Generates the password hash.
	 * @param string password
	 * @param string salt
	 * @return string hash
	 */
	public function hashPassword($password,$salt)
	{
		return md5($salt.$password);
	}
	/**
	 * Generates a salt that can be used to generate a password hash.
	 * @return string the salt
	 */
	public function generateSalt()
	{
		return uniqid('',true);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idusuario',$this->idusuario);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('session',$this->session,true);
		$criteria->compare('username',$this->username,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuarios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
