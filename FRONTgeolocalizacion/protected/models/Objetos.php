<?php

/**
 * This is the model class for table "objetos".
 *
 * The followings are the available columns in table 'objetos':
 * @property integer $idobjeto
 * @property string $nombre
 * @property string $descripcion
 * @property string $direccion
 * @property string $latitud
 * @property string $longitud
 * @property string $zoom
 * @property string $usuariocrea
 * @property string $creado
 * @property string $usuariomodifica
 * @property string $actualizado
 * @property string $ip
 * @property string $imagen1
 * @property string $imagen2
 * @property string $imagen3
 * @property boolean $status
 *
 * The followings are the available model relations:
 * @property Comentarios[] $comentarioses
 * @property Categoriaobjeto[] $categoriaobjetos
 * @property Objetoetiqueta[] $objetoetiquetas
 * @property Visitas[] $visitases
 * @property Atributoobjeto[] $atributoobjetos
 * @property Calificaciones[] $calificaciones
 */
class Objetos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'objetos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, descripcion, direccion, latitud, longitud, zoom', 'required'),
			array('nombre, usuariocrea, usuariomodifica, ip', 'length', 'max'=>80),
			array('descripcion', 'length', 'max'=>300),
			array('direccion, latitud, longitud', 'length', 'max'=>40),
			array('imagen, imagen2, imagen3', 'length', 'max'=>30),
			array('imagen, imagen2, imagen3',  'file', 'types'=>'jpg, gif, png', 'safe' => false),
			array('creado, actualizado, status', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idobjeto, nombre, descripcion, direccion, latitud, longitud, zoom, usuariocrea, creado, usuariomodifica, actualizado, ip, imagen, imagen2, imagen3, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'comentarioses' => array(self::HAS_MANY, 'Comentarios', 'idobjeto'),
			'categoriaobjetos' => array(self::HAS_MANY, 'Categoriaobjeto', 'idobjeto'),
			'objetoetiquetas' => array(self::HAS_MANY, 'Objetoetiqueta', 'idobjeto'),
			'visitases' => array(self::HAS_MANY, 'Visitas', 'idobjeto'),
			'atributoobjetos' => array(self::HAS_MANY, 'Atributoobjeto', 'idobjeto'),
			'calificaciones' => array(self::HAS_MANY, 'Calificaciones', 'idobjeto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idobjeto' => 'Idobjeto',
			'nombre' => 'Nombre',
			'descripcion' => 'Descripcion',
			'direccion' => 'Direccion',
			'latitud' => 'Latitud',
			'longitud' => 'Longitud',
			'zoom' => 'Zoom',
			'usuariocrea' => 'Usuariocrea',
			'creado' => 'Creado',
			'usuariomodifica' => 'Usuariomodifica',
			'actualizado' => 'Actualizado',
			'ip' => 'Ip',
			'imagen' => 'Imagen1',
			'imagen2' => 'Imagen2',
			'imagen3' => 'Imagen3',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idobjeto',$this->idobjeto);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('latitud',$this->latitud,true);
		$criteria->compare('longitud',$this->longitud,true);
		$criteria->compare('zoom',$this->zoom,true);
		$criteria->compare('usuariocrea',$this->usuariocrea,true);
		$criteria->compare('creado',$this->creado,true);
		$criteria->compare('usuariomodifica',$this->usuariomodifica,true);
		$criteria->compare('actualizado',$this->actualizado,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('imagen1',$this->imagen1,true);
		$criteria->compare('imagen2',$this->imagen2,true);
		$criteria->compare('imagen3',$this->imagen3,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Objetos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
