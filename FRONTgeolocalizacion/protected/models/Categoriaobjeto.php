<?php

/**
 * This is the model class for table "categoriaobjeto".
 *
 * The followings are the available columns in table 'categoriaobjeto':
 * @property integer $idcateobje
 * @property integer $idcategoria
 * @property integer $idobjeto
 * @property string $usuariocrea
 * @property string $creado
 * @property string $usuariomodifica
 * @property string $actualizado
 * @property string $ip
 *
 * The followings are the available model relations:
 * @property Categorias $idcategoria0
 * @property Objetos $idobjeto0
 */
class Categoriaobjeto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'categoriaobjeto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idcategoria, idobjeto', 'required'),
			array('idcategoria, idobjeto', 'numerical', 'integerOnly'=>true),
			array('usuariocrea, usuariomodifica, ip', 'length', 'max'=>80),
			array('creado, actualizado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idcateobje, idcategoria, idobjeto, usuariocrea, creado, usuariomodifica, actualizado, ip', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idcategoria0' => array(self::BELONGS_TO, 'Categorias', 'idcategoria'),
			'idobjeto0' => array(self::BELONGS_TO, 'Objetos', 'idobjeto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idcateobje' => 'Idcateobje',
			'idcategoria' => 'Idcategoria',
			'idobjeto' => 'Idobjeto',
			'usuariocrea' => 'Usuariocrea',
			'creado' => 'Creado',
			'usuariomodifica' => 'Usuariomodifica',
			'actualizado' => 'Actualizado',
			'ip' => 'Ip',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idcateobje',$this->idcateobje);
		$criteria->compare('idcategoria',$this->idcategoria);
		$criteria->compare('idobjeto',$this->idobjeto);
		$criteria->compare('usuariocrea',$this->usuariocrea,true);
		$criteria->compare('creado',$this->creado,true);
		$criteria->compare('usuariomodifica',$this->usuariomodifica,true);
		$criteria->compare('actualizado',$this->actualizado,true);
		$criteria->compare('ip',$this->ip,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Categoriaobjeto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
