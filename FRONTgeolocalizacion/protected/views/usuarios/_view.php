<?php
/* @var $this UsuariosController */
/* @var $data Usuarios */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idusuario')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idusuario), array('view', 'id'=>$data->idusuario)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('visitases')); ?>:</b>
	<?php 
	foreach ($data->visitases as $exp){
		echo $exp->idvisitas.'<br>';
	}
	?>
	<br />
	
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('comentarioses')); ?>:</b>
	<?php 
	foreach ($data->comentarioses as $exp){
		echo $exp->comentario.'<br>';
	}
	?>
	<br />
	
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('calificaciones')); ?>:</b>
	<?php 
	foreach ($data->calificaciones as $exp){
		echo $exp->calificacion.'<br>';
	}
	?>
	<br />
	
</div>

