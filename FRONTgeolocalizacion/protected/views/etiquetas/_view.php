<?php
/* @var $this EtiquetasController */
/* @var $data Etiquetas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idetiqueta')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idetiqueta), array('view', 'id'=>$data->idetiqueta)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('objetoetiquetas')); ?>:</b>
	<?php 
	foreach ($data->objetoetiquetas as $exp){
		echo $exp->idobjeetiq.'<br>';
	}
	?>
	<br />


</div>