<?php
/* @var $this ObjetosController */
/* @var $model Objetos */

$this->breadcrumbs=array(
	'Objetoses'=>array('index'),
	$model->idobjeto,
);

$this->menu=array(
	array('label'=>'List Objetos', 'url'=>array('index')),
	array('label'=>'Create Objetos', 'url'=>array('create')),
	array('label'=>'Update Objetos', 'url'=>array('update', 'id'=>$model->idobjeto)),
	array('label'=>'Delete Objetos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idobjeto),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Objetos', 'url'=>array('admin')),
);
?>



<h1>View Objetos #<?php echo $model->idobjeto; ?></h1>



<?php $this->widget('zii.widgets.CDetailView', array(
  'data'=>$model,
  'attributes'=>array(
    'idobjeto',
    'nombre',
    'descripcion',
    'direccion',
  ),
)); ?>




<div class="gllpLatlonPicker"> 
<fieldset class="gllpLatlonPicker" id="custom_id">

<div class="gllpMap">Google Maps</div>
<input type="hidden" class="gllpLatitude" value="<?= $model->latitud; ?>"/>
<input type="hidden" class="gllpLongitude" value="<?= $model->longitud; ?>"/>
<input type="hidden" class="gllpZoom" value="<?= $model->zoom; ?>"/>
</fieldset>
  </div>



<div class="form">
<?php if (Yii::app()->user->isGuest): ?>
  Para comentar debe iniciar sesion
<?php else: ?>

<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'comentarios-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'enableClientValidation'=>true,
  'clientOptions'=>array(
    'validateOnSubmit'=>true,
    )

)); ?>
<br />
<b><?php echo CHtml::encode($model->getAttributeLabel('Comentarios')); ?>:</b>
  <?php 
  foreach ($model->comentarioses as $exp){//muestra todas los comentarios
    echo $exp->comentario.'<br>';
  }
  ?>
  <br />


  

  <?php echo $form->errorSummary($comentarios); ?>
 <div class="row">     
  <?php echo $form->labelEx($comentarios,'comentario'); ?>
    <?php echo $form->textField($comentarios,'comentario',array('size'=>60,'maxlength'=>600)); ?>
    <?php echo $form->error($comentarios,'comentario'); ?>
  </div>
 
  <div class="row buttons">
    <?php echo CHtml::submitButton($comentarios->isNewRecord ? 'Create' : 'Save'); ?>
  </div>





<?php $this->endWidget(); ?>



<?php endif ?>


</div><!-- form -->


<div class="form">
<?php if (Yii::app()->user->isGuest): ?>
  Para calificar debe iniciar sesion
<?php else: ?>

<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'calificaciones-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
  'enableClientValidation'=>true,
  'clientOptions'=>array(
    'validateOnSubmit'=>true,
    )
)); ?>

  
  
  
  
  <b><?php echo CHtml::encode($model->getAttributeLabel('Calificaciones')); ?>:</b>
  <?php 
  foreach ($model->calificaciones as $exp){//muestra todas las calificaciones
    echo $exp->calificacion.'<br>';
  }
  ?>
  <br />




 <?php echo $form->errorSummary($calificaciones); ?>

  <div class="row">
    <?php echo $form->labelEx($calificaciones,'calificacion'); ?>
    <?php echo $form->textField($calificaciones,'calificacion');?>
    <?php echo $form->error($calificaciones,'calificacion'); ?>
  </div>


  <div class="row buttons">
    <?php echo CHtml::submitButton ($calificaciones->isNewRecord ? 'Create' : 'Save'); ?>
  </div>


 




<?php $this->endWidget(); ?>



<?php endif ?>


</div><!-- form -->

