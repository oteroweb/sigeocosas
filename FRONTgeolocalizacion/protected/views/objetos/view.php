<?php
/* @var $this ObjetosController */
/* @var $model Objetos */

$this->breadcrumbs=array(
	'Objetoses'=>array('index'),
	$model->idobjeto,
);

?> 
<div class="row">
  <div class="row">
      <ul class="col-md-12 etiquetas bolder text-center lista_etiquetaview" style=" padding: 0% 0% 0% 0%;"> 
    <?php foreach (Etiquetas::model()->findAll(array('order' => 'nombre ASC')) as $key => $value) : ?>
          <li class="mayus etiqueta_view" style="width:  auto ;"><span class="glyphicon <?= ($value['imagen'] == null) ? 'glyphicon-asterisk' : $value['imagen'];?>"></span>     <br><?= $value['nombre'] ?>     </li>
  <?php endforeach; ?>  
          </ul> 
    </div>
  <div class="col-md-12 view_carrousel">
      
<?php 
$url = "http://www.dailybitspro.com/sigeocosas/FRONTgeolocalizacion/images/objetos/";
 ?>


  <div class="row">
    <div class="col-md-12">
      <div class=" multi-item-carousel" id="theCarousel">
        <div class="carousel-inner">
          <div class="item">
            <div class="col-xs-4"><a href="#1"><img src="<?= $url.$model->idobjeto.$model->imagen ?>" class="img-responsive"></a></div>
          </div>
           <div class="item">
            <div class="col-xs-4"><a href="#1"><img src="<?= $url.$model->idobjeto.$model->imagen2 ?>" class="img-responsive"></a></div>
          </div>
           <div class="item">
            <div class="col-xs-4"><a href="#1"><img src="<?= $url.$model->idobjeto.$model->imagen3 ?>" class="img-responsive"></a></div>
          </div>
        </div>
        <a class="left carousel-control" href="#theCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
        <a class="right carousel-control" href="#theCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
      </div>
    </div>
  </div>


  </div>
<div class="col-md-12">
    <div class="col-md-6">
      <div class="col-md-6"><img  class="img-responsive" src="<?= $url.$model->idobjeto.$model->imagen ?>" alt="<?php echo CHtml::encode($model->descripcion); ?>" /> 
      </div>
      <div class="col-md-6 view_details">
        <h1 class="nombre_view"><?= $model->nombre ?> </h1>
        <h3 class="direccion_view"><?= $model->direccion ?> </h3>
        <h3 class="descripcion_view"><?= $model->descripcion ?> </h3>
      </div>
    </div>
  <div class="col-md-6">
   <div class="gllpLatlonPicker"> 
<fieldset class="gllpLatlonPicker" id="custom_id">

   <div class="gllpMap"></div>
<input type="hidden" class="gllpLatitude" value="<?= $model->latitud; ?>"/>
<input type="hidden" class="gllpLongitude" value="<?= $model->longitud; ?>"/>
<input type="hidden" class="gllpZoom" value="<?= $model->zoom; ?>"/>
</fieldset>
  </div>
  </div>
</div>
<?php 
     Yii::import('ext.gmap.*');
       $gMap = new EGMap(); $gMap->zoom = 13; $mapTypeControlOptions = array( 'position'=> EGMapControlPosition::LEFT_BOTTOM,   'style'=>EGMap::MAPTYPECONTROL_STYLE_DROPDOWN_MENU );
        $gMap->mapTypeControlOptions= $mapTypeControlOptions;
        $gMap->setCenter($model->latitud, $model->longitud);
        $info_window_a = new EGMapInfoWindow('<div>'.$model->descripcion.'</div>');
        $marker = new EGMapMarker($model->latitud,$model->longitud, array('title' => 'Marker With Custom Image'));
        $marker->addHtmlInfoWindow($info_window_a);
        $gMap->addMarker($marker);
        $gMap->renderMap();
     ?>

  <div class="col-md-12 white bolder text-center">
    <h1> <b><?php echo CHtml::encode($model->getAttributeLabel('Comentarios')); ?>:</b>  </h1> 
  </div>
  <div class="col-md-12">
    <?php foreach ($model->comentarioses as $exp): ?> 
      <?php if ($exp->idusuario==Yii::app()->user->getId()): ?>
      <div class="col-md-8 white"> <?=  "<h4>".$exp->comentario."</h4>"; ?> </div>
      <div class="col-md-2"> <?= CHtml::link('<i class="twa twa-pencil2"></i>', array('comentarios/update', 'id'=>$exp->idcomentario),array('class'=>'btn btn-lg btn-primary')); ?> </div>
      <div class="col-md-2"> <?= CHtml::link('<i class="twa twa-x"></i>', array('comentarios/delete', 'id'=>$exp->idcomentario,'redirect'=>  Yii::app()->createUrl('/comentarios/delete/id/'.$exp->idcomentario)),array('class'=>'btn btn-lg btn-primary',   /* comentado por error */ /* 'confirm' => '¿estas seguro de eliminar el comentario?'*/)); ?> </div>
      <?php  else:  ?> <div class="col-md-12 white"><h4><?= $exp->comentario;?></h4> </div> 
      <?php endif;?> 
    <?php  endforeach;   ?>
  </div>

<div class="col-md-12">
      <?php if (Yii::app()->user->isGuest): ?> Para comentar debe iniciar sesion <?php else: ?>
      <?php $form=$this->beginWidget('CActiveForm', array(   'id'=>'comentarios-form', 'enableAjaxValidation'=>false,   'enableClientValidation'=>true,   'clientOptions'=>array(     'validateOnSubmit'=>true,     ) ));  
     // echo $form->errorSummary($comentarios);
      ?>
     <div class="col-md-12 white bolder text-center"> <h2> <?= $form->labelEx($comentarios,'Deja tu comentario'); ?> </h2>   </div>
     <div class="col-md-12"> <?php // echo $form->error($comentarios,'comentario'); ?> </div>
     <div class="input-group">
        <?= $form->textField($comentarios,'comentario',array('size'=>60,'maxlength'=>600,'class'=>'form-control','placeholder' => 'Deja tu comentario',"value" => '')); ?> 
          <span class="input-group-btn"> 
          <?= CHtml::submitButton($comentarios->isNewRecord ? 'Comentar' : 'Comentar',array('class'=> "btn btn-primary",'type'=>"button" ));  ?> 
      </span>
    </div>
      <div class="col-md-12 text-center">   </div>
    <?php $this->endWidget(); ?>
    <?php endif; ?>
    </div>

  
  <div class="col-md-12"> 
  <h1>Calificaciones</h1>
    <?php if (Yii::app()->user->isGuest): ?>
      Para calificar debe iniciar sesion
      <?php  foreach ($model->calificaciones as $exp){ echo $exp->calificacion.'<br>'; }       ?>
    <?php else: ?>
      <?php $form=$this->beginWidget('CActiveForm', array(    'id'=>'calificaciones-form', 'enableAjaxValidation'=>false,    'enableClientValidation'=>true,   'clientOptions'=>array(     'validateOnSubmit'=>true,) )); ?>
            <b><?php echo CHtml::encode($model->getAttributeLabel('Calificaciones')); ?>:</b>
            <?php  foreach ($model->calificaciones as $exp){ echo $exp->calificacion.'<br>'; }       ?>
              <?php // echo $form->errorSummary($calificaciones); ?>
              <?php // echo $form->labelEx($calificaciones,'calificacion'); ?>
              <?php echo $form->textField($calificaciones,'calificacion');?>
              <?php // // echo $form->error($calificaciones,'calificacion'); ?>
              <?php  echo CHtml::submitButton ($calificaciones->isNewRecord ? 'Create' : 'Save'); ?>
      <?php $this->endWidget(); ?>
    <?php endif; ?>
  </div>

</div> <!-- row -->



<script>  
// Instantiate the Bootstrap carousel
$('.multi-item-carousel').carousel({
  interval: false
});

// for every slide in carousel, copy the next slide's item in the slide.
// Do the same for the next, next item.
$('.multi-item-carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  } else {
    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});
</script>


<style> 
#EGMapContainer1{display:none;}</style>