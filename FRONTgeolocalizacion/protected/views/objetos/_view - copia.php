<?php
/* @var $this ObjetosController */
/* @var $data Objetos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nombre')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->nombre), array('view', 'id'=>$data->idobjeto)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />



	
	<b><?php echo CHtml::encode($data->getAttributeLabel('Categorias')); ?>:</b>
	<?php 
		$categoriaobjetos=Categorias::model()->findAllBySql('select categorias.nombre from categoriaobjeto INNER JOIN categorias on categoriaobjeto.idcategoria = categorias.idcategoria where categoriaobjeto.idobjeto ='.$data->idobjeto);
foreach ($categoriaobjetos as $key => $value) {
	echo $value['nombre'].'<br>';
}

	?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('Atributos')); ?>:</b>
	<?php 
		$atributoobjetos=Atributos::model()->findAllBySql('select atributos.nombre 
from atributoobjeto INNER JOIN atributos on atributoobjeto.idatributo = atributos.idatributo where atributoobjeto.idobjeto ='.$data->idobjeto);
foreach ($atributoobjetos as $key => $value) {
	echo $value['nombre'].'<br>';
}

	?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('Etiquetas')); ?>:</b>
	<?php 
		$objetoetiquetas=Etiquetas::model()->findAllBySql('select etiquetas.nombre 
from objetoetiqueta INNER JOIN etiquetas on objetoetiqueta.idetiqueta = etiquetas.idetiqueta where objetoetiqueta.idobjeto ='.$data->idobjeto);
foreach ($objetoetiquetas as $key => $value) {
	echo $value['nombre'].'<br>';
}

	?>
</div>





<style type="text/css">
.labels {
   color: red;
   background-color: white;
   font-family: "Lucida Grande", "Arial", sans-serif;
   font-size: 10px;
   font-weight: bold;
   text-align: center;
   width: 40px;     
   border: 2px solid black;
   white-space: nowrap;
}
</style>
