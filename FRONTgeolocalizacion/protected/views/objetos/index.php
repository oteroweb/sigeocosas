	<?php


/* @var $this ObjetosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Objetos',
);

// $this->menu=array(
// 	array('label'=>'Create Objetos', 'url'=>array('create')),
// 	array('label'=>'Manage Objetos', 'url'=>array('admin')),
// );
?>


<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-6 sites">
		<?php $i=1; ?>
		<?php if (isset($_POST['Categoriaobjeto']) or isset($_GET['nombre']) ): ?>
			<?php foreach ($todos as $key => $valor): ?>
				<div class="col-md-6 col-sm-6 col-xs-12 objeto_global">
					<div class="col-md-4 col-xs-4 vista_index_categoria" style="padding-right: 0px; padding-left: 0px; ">
					<img  class="img-responsive" src="<?= Yii::app()->baseUrl; ?>/images/default.png" alt="<?php echo CHtml::encode($valor['descripcion']); ?>" /> 
					</div>
					<div class="col-md-8 col-xs-8">
						<h4 class="titulo_objeto_vista"> <?= CHtml::link(CHtml::encode($valor['nombre']), array('view', 'id'=>$valor['idobjeto'])); ?> </h4> 
						<b><?php // echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?></b>
						<?= 'Descripcion: '.CHtml::encode($valor['descripcion']); ?>
						<br />
						<b><?php // echo CHtml::encode($data->getAttributeLabel('Direccion')); ?></b>
						<?= 'Direccion: '.CHtml::encode($valor['direccion']); ?>
						<br />
						<b><?php // echo CHtml::encode($data->getAttributeLabel('Atributos')); ?></b>
						<?php $atributoobjetos=Atributos::model()->findAllBySql('select atributos.nombre from atributoobjeto INNER JOIN atributos on atributoobjeto.idatributo = atributos.idatributo where atributoobjeto.idobjeto ='.$valor['idobjeto']);
						foreach ($atributoobjetos as $key => $value) {echo $value['nombre'].'<br>';	}			?>
						
						<b><?php // echo CHtml::encode($data->getAttributeLabel('Categorias')); ?></b>
						<?php $categoriaobjetos=Categorias::model()->findAllBySql('select categorias.nombre from categoriaobjeto INNER JOIN categorias on categoriaobjeto.idcategoria = categorias.idcategoria where categoriaobjeto.idobjeto ='.$valor['idobjeto']);
						foreach ($categoriaobjetos as $key => $value) { echo $value['nombre'].'/';	} 	?>
						 <b><?php // echo CHtml::encode($data->getAttributeLabel('Etiquetas')); ?></b> <br>
						<?php 
						$objetoetiquetas=Etiquetas::model()->findAllBySql('select etiquetas.nombre , etiquetas.idetiqueta from objetoetiqueta INNER JOIN etiquetas on objetoetiqueta.idetiqueta = etiquetas.idetiqueta where objetoetiqueta.idobjeto ='.$valor['idobjeto']); 
						foreach ($objetoetiquetas as $key => $value) { echo CHtml::link(CHtml::encode($value->nombre), array('index', 'nombre'=>$value['idetiqueta'])).'<br>'; 	} ?>
					</div>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<?php  $this->widget('zii.widgets.CListView', array('dataProvider'=>$dataProvider,'itemView'=>'_view', ),array()); ?>
		<?php endif ?>
	</div>
	<?php
	 	?>
	<div class="floatmap col-md-6 col-sm-6 col-xs-6  maps">
		<?php 	
		$gMap = new EGMap(); $gMap->zoom = 13; $mapTypeControlOptions = array( 'position'=> EGMapControlPosition::LEFT_BOTTOM,  
		 // 'style'=>EGMap::MAPTYPECONTROL_STYLE_DROPDOWN_MENU 
		 );
				$gMap->mapTypeControlOptions= $mapTypeControlOptions;
				$gMap->setCenter(5.4727084, -74.7030036);
				foreach ($todos as $key => $value) {
				$info_window_a = new EGMapInfoWindow('<div>'.$value['descripcion'].'</div>');
				$marker = new EGMapMarker($value['latitud'], $value['longitud'], array('title' => 'Marker With Custom Image'));
				$marker->addHtmlInfoWindow($info_window_a);
				$gMap->addMarker($marker);
				}
		 		$gMap->renderMap();
		 ?>
	</div>
</div>
