<?php

$this->pageTitle = 'recuperar password';
$this->breadcrumbs = array('recuperar password');
echo $msg;
?>
<?php if (isset($_GET['token'])): ?>
  <?php if ($validToken): ?>
<div class="form">
    <?php $form = $this->beginWidget ('CActiveForm',
 array(
     'method' => 'post',
     'action' => yii::app()->createUrl('site/newpass'),
     'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
 )); 
?>
     <div class="row">
        <?php echo $form->labelEx($model,'nueva'); ?>
        <?php echo $form->textField($model,'nueva'); ?>
        <?php echo $form->error($model,'nueva'); ?>
    </div>
    <div class="row" style="display:none;">
        <?php echo $form->labelEx($model,'mail'); ?>
        <?php echo $form->textField($model,'id',array('value'=>$_GET['id'])); ?>
        <?php echo $form->error($model,'mail'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'repetir'); ?>
        <?php echo $form->textField($model,'repetir'); ?>
        <?php echo $form->error($model,'repetir'); ?>
    </div>
        
    
    <div class="row ">
        <?php echo CHtml::submitButton('recuperar password', array('class' => 'btn btn-primary')); ?>
    </div>
   
    <?php $this->endWidget();?>
</div>

    <?php else: ?>
    No es valido vuelva a solicitar contraseñ
  <?php endif ?>

<?php else: ?>  
<div class="form">
    <?php $form = $this->beginWidget ('CActiveForm',
 array(
     'method' => 'post',
     'action' => yii::app()->createUrl('site/recuperarpassword'),
     'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
 )); 
?>
    
    
     <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username'); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>
    
     <div class="row">
        <?php
                echo $form->labelEX($model,'captcha');
                $this->widget('CCaptcha', array('buttonLabel'=>'Actulizar codigo'));
                echo $form->textField($model, 'captcha');
                ?>
          <div class="text-info">
        </div>
         <?php echo $form->error($model,'captcha', array ('class' => 'text-error'));?>
    </div>
    
    <div class="row ">
        <?php echo CHtml::submitButton('recuperar password', array('class' => 'btn btn-primary')); ?>
    </div>
   
    <?php $this->endWidget();?>
</div>
<?php endif ?>


