<?php
/* @var $this CalificacionesController */
/* @var $data Calificaciones */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idcalificacion')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idcalificacion), array('view', 'id'=>$data->idcalificacion)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('calificacion')); ?>:</b>
	<?php echo CHtml::encode($data->calificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idobjeto')); ?>:</b>
	<?php echo CHtml::encode($data->idobjeto0->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idusuario')); ?>:</b>
	<?php echo CHtml::encode($data->idusuario0->nombre); ?>
	<br />


</div>