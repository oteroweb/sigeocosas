<?php
/* @var $this CalificacionesController */
/* @var $model Calificaciones */

$this->breadcrumbs=array(
	'Calificaciones'=>array('index'),
	$model->idcalificacion=>array('view','id'=>$model->idcalificacion),
	'Update',
);

$this->menu=array(
	array('label'=>'List Calificaciones', 'url'=>array('index')),
	array('label'=>'Create Calificaciones', 'url'=>array('create')),
	array('label'=>'View Calificaciones', 'url'=>array('view', 'id'=>$model->idcalificacion)),
	array('label'=>'Manage Calificaciones', 'url'=>array('admin')),
);
?>

<h1>Update Calificaciones <?php echo $model->idcalificacion; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>