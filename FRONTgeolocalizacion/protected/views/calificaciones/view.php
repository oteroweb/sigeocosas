<?php
/* @var $this CalificacionesController */
/* @var $model Calificaciones */

$this->breadcrumbs=array(
	'Calificaciones'=>array('index'),
	$model->idcalificacion,
);

$this->menu=array(
	array('label'=>'List Calificaciones', 'url'=>array('index')),
	array('label'=>'Create Calificaciones', 'url'=>array('create')),
	array('label'=>'Update Calificaciones', 'url'=>array('update', 'id'=>$model->idcalificacion),'visible' =>$model->userIsAuthor(),),
	array('label'=>'Delete Calificaciones', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idcalificacion),'confirm'=>'Are you sure you want to delete this item?'),'visible' =>$model->userIsAuthor(),),
	array('label'=>'Manage Calificaciones', 'url'=>array('admin')),
);
?>

<h1>View Calificaciones #<?php echo $model->idcalificacion; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idcalificacion',
		'calificacion',
		'idobjeto',
		'idusuario',
	),
)); ?>
