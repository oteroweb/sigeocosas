<?php
/* @var $this CalificacionesController */
/* @var $model Calificaciones */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idcalificacion'); ?>
		<?php echo $form->textField($model,'idcalificacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'calificacion'); ?>
		<?php echo $form->textField($model,'calificacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idobjeto'); ?>
		<?php echo $form->textField($model,'idobjeto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idusuario'); ?>
		<?php echo $form->textField($model,'idusuario'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->