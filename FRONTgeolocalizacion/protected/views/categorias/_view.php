<?php
/* @var $this CategoriasController */
/* @var $data Categorias */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idcategoria')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idcategoria), array('view', 'id'=>$data->idcategoria)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('categoriaobjetos')); ?>:</b>
	<?php 
	foreach ($data->categoriaobjetos as $exp){
		echo $exp->idcateobje.'<br>';
	}
	?>
	<br />


</div>