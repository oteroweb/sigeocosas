
<?php
/* @var $this ComentariosController */
/* @var $model Comentarios */

$this->breadcrumbs=array(
	'Comentarioses'=>array('index'),
	$model->idcomentario,
);

$this->menu=array(
	array('label'=>'List Comentarios', 'url'=>array('index')),
	array('label'=>'Create Comentarios', 'url'=>array('create')),

	array('label'=>'Update Comentarios', 'url'=>array('update', 'id'=>$model->idcomentario),//actualiza los comenatrios tan solo de quien estan en session.
		'visible' =>$model->userIsAuthor(),),

	array('label'=>'Manage Comentarios', 'url'=>array('admin')),
	
	array('label'=>'Delete Comentarios', 'url'=>'#', 'linkOptions'=>array(
		'submit'=>array(
			'delete',
			'id'=>$model->idcomentario //elimina los comenatrios tan solo de quien estan en session.
		),
		'confirm'=>'Are you sure you want to delete this item?',
		
	),'visible' =>$model->userIsAuthor(),),
);


?>

<h1>View Comentarios #<?php echo $model->idcomentario; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idcomentario',
		'comentario',
		'idobjeto',
		'idusuario',
	),
)); ?>
