<?php
/* @var $this AtributoobjetoController */
/* @var $model Atributoobjeto */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idatriobje'); ?>
		<?php echo $form->textField($model,'idatriobje'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idatributo'); ?>
		<?php echo $form->textField($model,'idatributo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idobjeto'); ?>
		<?php echo $form->textField($model,'idobjeto'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->