<?php
/* @var $this AtributoobjetoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Atributoobjetos',
);

$this->menu=array(
	array('label'=>'Create Atributoobjeto', 'url'=>array('create')),
	array('label'=>'Manage Atributoobjeto', 'url'=>array('admin')),
);
?>

<h1>Atributoobjetos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
