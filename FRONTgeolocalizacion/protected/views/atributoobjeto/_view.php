<?php
/* @var $this AtributoobjetoController */
/* @var $data Atributoobjeto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idatriobje')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idatriobje), array('view', 'id'=>$data->idatriobje)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idatributo')); ?>:</b>
	<?php echo CHtml::encode($data->idatributo0->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idobjeto')); ?>:</b>
	<?php echo CHtml::encode($data->idobjeto0->nombre); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b> 
	<?php echo CHtml::encode($data->idobjeto0->descripcion); ?>
	<br />


</div>