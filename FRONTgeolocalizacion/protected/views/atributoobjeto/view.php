<?php
/* @var $this AtributoobjetoController */
/* @var $model Atributoobjeto */

$this->breadcrumbs=array(
	'Atributoobjetos'=>array('index'),
	$model->idatriobje,
);

$this->menu=array(
	array('label'=>'List Atributoobjeto', 'url'=>array('index')),
	array('label'=>'Create Atributoobjeto', 'url'=>array('create')),
	array('label'=>'Update Atributoobjeto', 'url'=>array('update', 'id'=>$model->idatriobje)),
	array('label'=>'Delete Atributoobjeto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idatriobje),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Atributoobjeto', 'url'=>array('admin')),
);
?>

<h1>View Atributoobjeto #<?php echo $model->idatriobje; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idatriobje',
		'idatributo',
		'idobjeto',
	),
)); ?>
