<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
	<script src="<?= Yii::app()->baseUrl; ?>/mapa/js/jquery-2.1.1.min.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
	<link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl; ?>/css/jquery-gmaps-latlon-picker.css"/>
	<script src="<?= Yii::app()->baseUrl; ?>/mapa/js/jquery-gmaps-latlon-picker.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<!-- blueprint CSS framework -->
	

	<!-- CSS and JS for our code -->
	<?php
	// echo yii::app()->bootstrap->registerAllCss();
	// echo yii::app()->bootstrap->registerCoreScripts();
	?>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
	<div id="header">
	</div><!-- header -->

	<nav class="navbar navbar-gris">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#colapsado" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="colapsado">
      <ul class="nav navbar-nav">
        
      </ul>
      <form class="navbar-form navbar-right">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Email">
          <input type="text" class="form-control" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


<div class="row">
  <div class="col-md-12">
   <ul class=" etiquetas"> 
   <li>     <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>     <br>mi elemento     </li>
   <li>     <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>     <br>mi elemento     </li>
   <li>     <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>     <br>mi elemento     </li>
   <li>     <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>     <br>mi elemento     </li>
   <li>     <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>     <br>mi elemento     </li>
       </ul> 
       </div>
</div>

</body>
</html>



