<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <!-- <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script> -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/locationpicker.jquery.min.js"></script>
	<script src="<?= Yii::app()->baseUrl; ?>/js/jquery-2.1.1.min.js"></script>
	<!-- <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> -->
	<link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl; ?>/css/jquery-gmaps-latlon-picker.css"/>
	<script src="<?= Yii::app()->baseUrl; ?>/js/jquery-gmaps-latlon-picker.js"></script>
	<link rel="stylesheet" href="<?= Yii::app()->baseUrl; ?>/css/bootstrap.min.css">
	<script src="<?= Yii::app()->baseUrl; ?>/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl; ?>/css/style.css"/>
	<link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl; ?>/css/miestilo.css"/>
	<link rel="stylesheet" href="<?= Yii::app()->baseUrl; ?>/css/twemoji-awesome.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>


</head>

<body>
 
   <nav class="navbar navbar-grey navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <!-- <a class="navbar-brand" href="http://getbootstrap.com/examples/carousel/#">Project name</a> -->
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">

                <?php if (!Yii::app()->user->isGuest): ?>
                	<li>
                		
							<h3>Bienvenido <?= Yii::app()->user->getName() ?> &nbsp;</h3> 
                	</li>
							 <li><?= CHtml::link('Cerrar sesion',array('site/logout'), array('class'=>'btn btn-primary', 'role' => 'button')); ?></li>
						<?php else: ?>
							<form class="navbar-form" action="<?= Yii::app()->createUrl('site/login') ?>" method="POST">
		            <div class="form-group">
		              <input type="text" placeholder="Usuario" class="form-control" name="LoginForm[username]" autocomplete="off">
		            </div>
		            <div class="form-group">
		              <input type="password" placeholder="Contraseña" class="form-control" autocomplete="off" name="LoginForm[password]">
		            </div>
		            <button type="submit" class=" yellow_button bolder black btn btn-success">LOGIN</button>
		          </form>
						<?php endif ?>
		          
              
              </ul>
              
              </a>
            </div>
          </div>
        </nav>
   

    <!-- Main jumbotron for a primary marketing message or call to action -->
    

 <div class="container">
		<div class="page-header">
	                <br /> <br />
			<?php if(isset($this->breadcrumbs)):?>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
					'links'=>$this->breadcrumbs,
				)); ?><!-- breadcrumbs -->
			<?php endif?>
		</div>
			<div class="row">
			<?php echo $content; ?>
			</div>
		
	</div>


    </div> <!-- /container -->
  

</body>
</html>

