<?php
/* @var $this AtributosController */
/* @var $data Atributos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idatributo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idatributo), array('view', 'id'=>$data->idatributo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo')); ?>:</b>
	<?php echo CHtml::encode($data->tipo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('caracter')); ?>:</b>
	<?php echo CHtml::encode($data->caracter); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('atributoobjetos')); ?>:</b>
	<?php 
	foreach ($data->atributoobjetos as $exp){
		echo $exp->idatriobje.'<br>';
	}
	?>
	<br />


</div>