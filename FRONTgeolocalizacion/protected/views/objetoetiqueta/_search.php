<?php
/* @var $this ObjetoetiquetaController */
/* @var $model Objetoetiqueta */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idobjeetiq'); ?>
		<?php echo $form->textField($model,'idobjeetiq'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idetiqueta'); ?>
		<?php echo $form->textField($model,'idetiqueta'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idobjeto'); ?>
		<?php echo $form->textField($model,'idobjeto'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->