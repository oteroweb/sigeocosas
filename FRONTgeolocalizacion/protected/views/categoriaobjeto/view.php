<?php
/* @var $this CategoriaobjetoController */
/* @var $model Categoriaobjeto */

$this->breadcrumbs=array(
	'Categoriaobjetos'=>array('index'),
	$model->idcateobje,
);

$this->menu=array(
	array('label'=>'List Categoriaobjeto', 'url'=>array('index')),
	array('label'=>'Create Categoriaobjeto', 'url'=>array('create')),
	array('label'=>'Update Categoriaobjeto', 'url'=>array('update', 'id'=>$model->idcateobje)),
	array('label'=>'Delete Categoriaobjeto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idcateobje),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Categoriaobjeto', 'url'=>array('admin')),
);
?>

<h1>View Categoriaobjeto #<?php echo $model->idcateobje; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idcateobje',
		'idcategoria',
		'idobjeto',
	),
)); ?>
