<?php
/* @var $this CategoriaobjetoController */
/* @var $model Categoriaobjeto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'categoriaobjeto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'idcategoria'); ?>
		<?php echo $form->dropDownlist($model,'idcategoria',CHtml::listData(Categorias::model()->findAll(),'idcategoria','nombre'),array('empty'=>'seleccione categoria')); ?>
		<?php echo $form->error($model,'idcategoria'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idobjeto'); ?>
		<?php echo $form->dropDownlist($model,'idobjeto',CHtml::listData(Objetos::model()->findAll(),'idobjeto','nombre'),array('empty'=>'seleccione objeto')); ?>
		<?php echo $form->error($model,'idobjeto'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->