<?php

class m160812_002255_documentos extends CDbMigration
{
	public function up()
	{
		$this->createTable("documentos", array(
    "id"=>"pk",
    "documento"=>"character varying(255) NOT NULL",
    "descripcion"=>"text NOT NULL",
    "idserie"=>"integer",
    "idsubserie"=>"integer",
    "archivo"=>"string",
    "usuariocrea"=>"character varying(15)",
    "fechacrea"=>"timestamp",
    "usuariomodifica"=>"character varying(15)",
    "fechamodifica"=>"timestamp",
), "");
	}

	public function down()
	{
		echo "m160812_002255_documentos does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}