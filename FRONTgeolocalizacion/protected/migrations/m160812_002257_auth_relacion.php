<?php

class m160812_002257_auth_relacion extends CDbMigration
{
	public function up()
	{
		$this->createTable("auth_relacion", array(
    "parent"=>"pk",
    "child"=>"integer",
), "");
	}

	public function down()
	{
		echo "m160812_002257_auth_relacion does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}