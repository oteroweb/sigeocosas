<?php

class m160809_224657_calificaciones extends CDbMigration
{
	public function up()
	{
			
$this->createTable("documentos", array(
    "id"=>"pk",
    "documento"=>"character varying(255) NOT NULL",
    "descripcion"=>"text NOT NULL",
    "idserie"=>"integer",
    "idsubserie"=>"integer",
    "archivo"=>"string",
    "usuariocrea"=>"character varying(15)",
    "fechacrea"=>"timestamp",
    "usuariomodifica"=>"character varying(15)",
    "fechamodifica"=>"timestamp",
), "");

$this->createTable("ha_logins", array(
    "id"=>"pk",
    "userid"=>"integer NOT NULL",
    "loginprovider"=>"character varying(50) NOT NULL",
    "loginprovideridentifier"=>"character varying(100) NOT NULL",
), "");

$this->createTable("imagen", array(
    "id"=>"pk",
    "foto"=>"character varying(200) NOT NULL",
), "");

$this->createTable("etiquetas", array(
    "idetiqueta"=>"pk",
    "nombre"=>"character varying(80) NOT NULL",
    "usuario"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");

$this->createTable("visitas", array(
    "idvisitas"=>"pk",
    "idobjeto"=>"integer NOT NULL",
    "idusuario"=>"integer NOT NULL",
    "usuario"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");

$this->createTable("auth_asignacion", array(
    "itemname"=>"pk",
    "userid"=>"pk",
    "bizrule"=>"text",
    "data"=>"text",
), "");

$this->createTable("atributoobjeto", array(
    "idatriobje"=>"pk",
    "idatributo"=>"integer NOT NULL",
    "idobjeto"=>"integer NOT NULL",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");

$this->createTable("auth_items", array(
    "name"=>"pk",
    "type"=>"integer NOT NULL",
    "description"=>"text",
    "bizrule"=>"text",
    "data"=>"text",
), "");

$this->createTable("auth_relacion", array(
    "parent"=>"pk",
    "child"=>"pk",
), "");

$this->createTable("atributos", array(
    "idatributo"=>"pk",
    "nombre"=>"character varying(80) NOT NULL",
    "tipo"=>"character varying(50) NOT NULL",
    "caracter"=>"character varying(50) NOT NULL",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");

$this->createTable("comentarios", array(
    "idcomentario"=>"pk",
    "comentario"=>"character varying(600) NOT NULL",
    "idobjeto"=>"integer NOT NULL",
    "idusuario"=>"integer NOT NULL",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");

$this->createTable("categoriaobjeto", array(
    "idcateobje"=>"pk",
    "idcategoria"=>"integer NOT NULL",
    "idobjeto"=>"integer NOT NULL",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");

$this->createTable("objetoetiqueta", array(
    "idobjeetiq"=>"pk",
    "idetiqueta"=>"integer NOT NULL",
    "idobjeto"=>"integer NOT NULL",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");

$this->createTable("calificaciones", array(
    "idcalificacion"=>"pk",
    "calificacion"=>"integer NOT NULL",
    "idobjeto"=>"integer NOT NULL",
    "idusuario"=>"integer NOT NULL",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");

$this->createTable("categorias", array(
    "idcategoria"=>"pk",
    "nombre"=>"character varying(80) NOT NULL",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");

$this->createTable("usuarios", array(
    "idusuario"=>"pk",
    "nombre"=>"character varying(80) NOT NULL",
    "password"=>"character varying(255) NOT NULL",
    "email"=>"character varying(100) NOT NULL",
    "username"=>"character varying(255) NOT NULL",
    "session"=>"text",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");

$this->createTable("objetos", array(
    "idobjeto"=>"pk",
    "nombre"=>"character varying(80) NOT NULL",
    "descripcion"=>"character varying(300) NOT NULL",
    "direccion"=>"character varying(40) NOT NULL",
    "latitud"=>"character varying(40) NOT NULL",
    "longitud"=>"character varying(40) NOT NULL",
    "zoom"=>"numeric NOT NULL",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp without time zone",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp without time zone",
    "ip"=>"character varying(80)",
), "");

	}

	public function down()
	{
			 $this->dropTable('atributos');
		
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}