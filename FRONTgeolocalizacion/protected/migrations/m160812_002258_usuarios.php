<?php

class m160812_002258_usuarios extends CDbMigration
{
	public function up()
	{
		
$this->createTable("usuarios", array(
    "idusuario"=>"pk",
    "nombre"=>"character varying(80) NOT NULL",
    "password"=>"character varying(255) NOT NULL",
    "email"=>"character varying(100) NOT NULL",
    "username"=>"character varying(255) NOT NULL",
    "session"=>"text",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
    "token"=>"character varying(80)",
), "");
	}

	public function down()
	{
		echo "m160812_002258_usuarios does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}