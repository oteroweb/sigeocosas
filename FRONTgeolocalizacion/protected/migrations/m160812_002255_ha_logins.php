<?php

class m160812_002255_ha_logins extends CDbMigration
{
	public function up()
	{
		
$this->createTable("ha_logins", array(
    "id"=>"pk",
    "userid"=>"integer NOT NULL",
    "loginprovider"=>"character varying(50) NOT NULL",
    "loginprovideridentifier"=>"character varying(100) NOT NULL",
), "");
	}

	public function down()
	{
		echo "m160812_002255_ha_logins does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}