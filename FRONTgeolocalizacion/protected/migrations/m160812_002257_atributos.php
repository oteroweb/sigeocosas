<?php

class m160812_002257_atributos extends CDbMigration
{
	public function up()
	{
		

$this->createTable("atributos", array(
    "idatributo"=>"pk",
    "nombre"=>"character varying(80) NOT NULL",
    "tipo"=>"character varying(50) NOT NULL",
    "caracter"=>"character varying(50) NOT NULL",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");
	}

	public function down()
	{
		echo "m160812_002257_atributos does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}