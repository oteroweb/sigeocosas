<?php

class m160812_002256_atributoobjeto extends CDbMigration
{
	public function up()
	{
		$this->createTable("atributoobjeto", array(
    "idatriobje"=>"pk",
    "idatributo"=>"integer NOT NULL",
    "idobjeto"=>"integer NOT NULL",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");
	}

	public function down()
	{
		echo "m160812_002256_atributoobjeto does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}