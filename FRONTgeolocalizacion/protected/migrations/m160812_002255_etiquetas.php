<?php

class m160812_002255_etiquetas extends CDbMigration
{
	public function up()
	{
		
$this->createTable("etiquetas", array(
    "idetiqueta"=>"pk",
    "nombre"=>"character varying(80) NOT NULL",
    "imagen"=>"character varying(20) NOT NULL",
    "usuario"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");
	}

	public function down()
	{
		echo "m160812_002255_etiquetas does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}