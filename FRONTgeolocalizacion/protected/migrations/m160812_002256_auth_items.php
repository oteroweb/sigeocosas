<?php

class m160812_002256_auth_items extends CDbMigration
{
	public function up()
	{
		$this->createTable("auth_items", array(
    "name"=>"pk",
    "type"=>"integer NOT NULL",
    "description"=>"text",
    "bizrule"=>"text",
    "data"=>"text",
), "");
	}

	public function down()
	{
		echo "m160812_002256_auth_items does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}