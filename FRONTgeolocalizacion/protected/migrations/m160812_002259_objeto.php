<?php

class m160812_002259_objeto extends CDbMigration
{
	public function up()
	{
		
$this->createTable("objetos", array(
    "idobjeto"=>"pk",
    "nombre"=>"character varying(80) NOT NULL",
    "descripcion"=>"character varying(300) NOT NULL",
    "imagen"=>"character varying(300) NOT NULL",
    "imagen2"=>"character varying(300) NOT NULL",
    "imagen3"=>"character varying(300) NOT NULL",
    "direccion"=>"character varying(40) NOT NULL",
    "latitud"=>"character varying(40) NOT NULL",
    "longitud"=>"character varying(40) NOT NULL",
    "zoom"=>"numeric NOT NULL",
    "usuariocrea"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp ",
    "status"=>"integer DEFAULT '1'",
    "ip"=>"character varying(80)",
), "");
	}

	public function down()
	{
		echo "m160812_002259_objeto does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}