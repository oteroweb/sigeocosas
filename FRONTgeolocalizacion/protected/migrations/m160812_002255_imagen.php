<?php

class m160812_002255_imagen extends CDbMigration
{
	public function up()
	{
		

$this->createTable("imagen", array(
    "id"=>"pk",
    "foto"=>"character varying(200) NOT NULL",
), "");
	}

	public function down()
	{
		echo "m160812_002255_imagen does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}