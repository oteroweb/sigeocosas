<?php

class m160812_002256_visitas extends CDbMigration
{
	public function up()
	{
		$this->createTable("visitas", array(
    "idvisitas"=>"pk",
    "idobjeto"=>"integer NOT NULL",
    "idusuario"=>"integer NOT NULL",
    "usuario"=>"character varying(80)",
    "creado"=>"timestamp",
    "usuariomodifica"=>"character varying(80)",
    "actualizado"=>"timestamp",
    "ip"=>"character varying(80)",
), "");
	}

	public function down()
	{
		echo "m160812_002256_visitas does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}