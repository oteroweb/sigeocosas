<?php

class m160812_033911_datos extends CDbMigration
{
	public function up()
	{
		$this->execute("
INSERT INTO `auth_asignacion` (`itemname`, `userid`, `bizrule`,`data`) VALUES 
('admin','44','',	'N'),
('super','61','',	'N'),
('admin','35','',	'N');

INSERT INTO  `usuarios` (`idusuario`, `nombre`, `password`, `email`, `username`, `session`, `usuariocrea`, `creado`, `usuariomodifica`, `actualizado`, `ip`) VALUES
(61,'jaco',	'4cb1e66a2da66ac0644061dd27350d31',	'jaco','jaco',	'55f1f827342547.12672295',	'sara',	'2015-09-10 21:37:43',	'sara',	'2015-09-10 21:37:43',	'::1');


INSERT INTO  `categorias`  (`idcategoria`, `nombre`, `usuariocrea`, `creado`, `usuariomodifica`, `actualizado`, `ip`) VALUES
(18,'Religion',	'jaco',	'2016-02-09 05:47:08',	'jaco',	'2016-02-09 05:47:08',	'::1'),
(17,'Educacion',	'jaco',	'2016-02-09 05:45:18',	'jaco',	'2016-02-09 05:47:27',	'::1'),
(19,'Diversion',	'jaco',	'2016-02-09 05:47:39',	'jaco',	'2016-02-09 05:47:39',	'::1'),
(20,'Atenciónciudadana',	'jaco',	'2016-02-09 05:47:49',	'jaco',	'2016-02-09 05:47:49',	'::1'),
(21,'Señalestransito',	'jaco',	'2016-02-09 05:48:50',	'jaco',	'2016-02-09 05:48:50',	'::1');

INSERT INTO `etiquetas` 
(`idetiqueta`, 	`nombre`, 	`usuario`, `creado`, `usuariomodifica`, `actualizado`, `ip`) VALUES
(5,				'compras','jaco','jaco',	'2016-02-09 06:29:27','2016-02-09 06:29:27', '::1'),
(6,				'juegos','jaco','jaco',	'2016-02-09 06:29:35','2016-02-09 06:29:35', '::1'),
(7,				'recreacion','jaco','jaco',	'2016-02-09 06:29:44','2016-02-09 06:29:44', '::1'),
(8,				'deporte','jaco','jaco',	'2016-02-09 06:29:52','2016-02-09 06:29:52', '::1'),
(9,				'transito','jaco','jaco',	'2016-02-09 06:30:06','2016-02-09 06:30:06', '::1'),
(10,			'salud','jaco','jaco',	'2016-02-09 06:30:32','2016-02-09 06:30:32', '::1');




");
	}

	public function down()
	{
		echo "m160812_033911_datos does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}