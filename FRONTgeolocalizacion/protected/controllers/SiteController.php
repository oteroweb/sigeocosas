<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
			public function actionIndex()
	{			$model = new Categoriaobjeto;
				// mail('nadie@example.com', 'El título', 'El mensaje', null, '-fwebmaster@example.com');
                if(isset($_POST['Categoriaobjeto'])) {
                	// if(false) {
                	// echo "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>";
                    // var_dump($_POST);
                //     $ha = Yii::app()->getModule('hybridauth')->getHybridAuth();
                //     $facebook = $ha->getAdapter('facebook');
                //     $user = $facebook->getUserProfile();
                //     var_dump($user->email);
				Yii::app()->runController('objetos/index');
                }
                else {
		$this->render('index', array('model'=>$model));
                }
                	

		
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	

	public function actionRecuperarpassword(){
            // echo "<br><br><br><br><br><br><br><br><br><br>";
            $model = new RecuperarPassword;
            $msg = '';
          
            if (isset($_POST["RecuperarPassword"]))
            {
                $model->attributes =$_POST['RecuperarPassword'];
                if(!$model->validate())
                {
                  $msg ="<strong class='text-error>error al enviar el formulario</strong>";  
                }
                else
                {

                    $conexion = yii::app()->db;
                    $consulta ="SELECT email,username,idusuario FROM usuarios WHERE";
                    $consulta .=" email='$model->email' AND username='$model->username' ";
                    $resultado = $conexion->createCommand($consulta);
                    $filas = $resultado->queryAll();
                    $existe=false;
                    if(count($filas))  { $existe = true; }
                    $idusuario= $filas[0]['idusuario'];
                    //si el usuario existe
                    $token = rand(1,5000);
                    if ($existe ===true)
                    {
                        
                        //URGENTE hacer esta consulta  ne la dB ALTER TABLE public.usuarios ADD COLUMN token character varying(80);
						$user = Usuarios::model()->findByAttributes(array(  'idusuario' => $idusuario));
           				$user->token= $token;
           				$user->save();
// var_dump($user);
                       
                       // $email = new EnviarEmail;
                       // $subject = "has solicitado recuperar password en";
                       // $subject .=yii::app()->name;
                       // $message = "bienvenid@" . $model->username . " su password es ";
                       // $message .=$password;
                       // $message .="<br /><br />";
// URGENTE Este es el mensaje real las dos siguientes lineas
                      $message = "para Recuperar tu contraseña ingresa al siguiente link";
                       $message .="<a href='http://localhost/sigeocosas/FRONTgeolocalizacion/site/recuperarpassword/id/".$idusuario."/token/".$token."'> regresar a la web</a>";
echo $message;

                      
                       // $email->Enviar_Email
                       //         (
                       //        array(Yii::app()->params['adminEmail'],yii::app()->name),
                       //         array($model->email,$model->username),
                       //         $subject,
                       //         $message
                       //         );
                       // $model->email ='';
                       // $model->username ='';
                       // $model->captcha ='';
                       // $msg ="<strong class='text-info>'hemos enviado su password</strong>";
                    }
                    else
                    {
                        $msg = "<strong class='test-error>error, el usuario no existe</strong>'";
                    }
                }
            }
            $validToken=$this->isValidToken();
		$this->render('recuperarpassword', array('model' => $model, 'msg' =>$msg, 'validToken' =>$validToken));
          // var_dump($_POST);    

	}
	public function isValidToken(){
if (isset($_GET['id'], $_GET['token'])) {
 $id = $_GET['id']; 
 $token = $_GET['token']; 
// $hayusu = Usuarios::model()->findByPk($id);
						$hayusu = Usuarios::model()->findByAttributes(array(  'idusuario' => $id,  'token' => $token));
// var_dump($hayusu);
                    if(!is_null($hayusu))  { return true; }
					else { return false; }
}
}
	public function actionNewPass(){
if (isset($_POST["RecuperarPassword"]))
            {
            $usuarionuevopass = Usuarios::model()->findByPk($_POST["RecuperarPassword"]['id']);

           //URGENTE PONER METDO DE ENCRIPTACION AQI
           $usuarionuevopass->password= "nuevo";
           	$usuarionuevopass->save();
           	//Urgente Poner codigo para indicar que cambio el password
            }

            }


public function actionTop10()
	{
		//poner en seccion SELECT de tu consulta los campos que quieres consultar
		$top10 = Objetos::model()->findallBySql('SELECT idobjeto, AVG(calificacion) FROM calificaciones GROUP BY idobjeto limit 10');
		$this->render('top10', array('top10'=>$top10));
	}




	

	
}



