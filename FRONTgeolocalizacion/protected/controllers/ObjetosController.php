<?php
class ObjetosController extends Controller
{
	

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','admin'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','create','update','delete'),
				'roles'=>array('admin'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','create','update','delete','Assign'),
				'roles'=>array('super'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{

		$comentarios=new Comentarios;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Comentarios']))
		{
			$comentarios->attributes=$_POST['Comentarios'];
			// $comentarios->save();
// var_dump($comentarios->attributes); 
			// echo "comentarios";
			if($comentarios->save());
				// $this->redirect(array('view','id'=>$comentarios->idcomentario));
	 
		} 

		


		$calificaciones=new Calificaciones;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Calificaciones']))
		{
			$calificaciones->attributes=$_POST['Calificaciones'];
			// $comentarios->save();
// var_dump($comentarios->attributes); 
			// echo "comentarios";
			if($calificaciones->save()){ echo "calificacion creado";}
				// $this->redirect(array('view','id'=>$comentarios->idcomentario));
		 
		} 


		$visitas = new Visitas;
$visitas->idobjeto=$_GET['id'];

if (Yii::app()->user->isGuest ) {
$visitas->idusuario=1;
}
else {
	$visitas->idusuario = Yii::app()->user->getId();
}

$visitas->ip=$this->getUserIp();
$visitas->save();

		$this->render('view',array(
			'comentarios'=>$comentarios,
			'calificaciones'=>$calificaciones,
			'visitas'=>$visitas,
			'model'=>$this->loadModel($id),
		));

		

	}
	


public function getUserIP()
{
      if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
            $addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
            return trim($addr[0]);
        } else {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    else {
        return $_SERVER['REMOTE_ADDR'];
    }
}









	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Objetos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Objetos']))
		{
			$model->attributes=$_POST['Objetos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idobjeto));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Objetos']))
		{
			$model->attributes=$_POST['Objetos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idobjeto));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
			if(isset($_POST['Categoriaobjeto']))
				{
					Yii::import('ext.gmap.*');
					// $dataProvider=new CActiveDataProvider('Objetos');
					$sql= 'select objetos.nombre,objetos.direccion,objetos.idobjeto,objetos.descripcion,objetos.latitud, objetos.longitud  from categoriaobjeto INNER JOIN objetos on categoriaobjeto.idobjeto = objetos.idobjeto where categoriaobjeto.idcategoria ='.$_POST['Categoriaobjeto']['idcategoria']. ' AND objetos.status = true';
			 		$categoriaobjetos=Objetos::model()->findAllBySql($sql);
			  		$dataProvider= new CSqlDataProvider($sql);
			  		// var_dump($dataProvider);
						$todos= array();
						$count=0;
					foreach ($dataProvider->getData() as $key => $value) {
						$todos[$count]['idobjeto']= $value["idobjeto"];
						$todos[$count]['nombre']= $value["nombre"];
						$todos[$count]['descripcion']= $value["descripcion"];
						$todos[$count]['latitud']= $value["latitud"];
						$todos[$count]['longitud']= $value["longitud"];
						$todos[$count]['direccion']= $value["direccion"];
						$count++;
					}	
					// var_dump($dataProvider);
						$this->render('index',array( 'dataProvider'=>$dataProvider, 'todos'=>$todos,));
				}
				elseif (isset($_GET['nombre'])) {
					// echo "solo";

					Yii::import('ext.gmap.*');
					// $dataProvider=new CActiveDataProvider('Objetos');
					$sql = 'select objetos.nombre,objetos.direccion,objetos.idobjeto,objetos.descripcion,objetos.latitud, objetos.longitud from objetoetiqueta INNER JOIN objetos on objetoetiqueta.idobjeto = objetos.idobjeto where objetoetiqueta.idetiqueta ='.$_GET['nombre']. ' AND objetos.status = true';
					// $sql= 'select objetos.nombre,objetos.direccion,objetos.idobjeto,objetos.descripcion,objetos.latitud, objetos.longitud  from categoriaobjeto INNER JOIN objetos on categoriaobjeto.idobjeto = objetos.idobjeto where categoriaobjeto.idcategoria ='.$_POST['Categoriaobjeto']['idcategoria'];
			 		$categoriaobjetos=Objetos::model()->findAllBySql($sql);
			  		$dataProvider= new CSqlDataProvider($sql);
			  		// var_dump($dataProvider);
						$todos= array();
						$count=0;
					foreach ($dataProvider->getData() as $key => $value) {
						$todos[$count]['idobjeto']= $value["idobjeto"];
						$todos[$count]['nombre']= $value["nombre"];
						$todos[$count]['descripcion']= $value["descripcion"];
						$todos[$count]['latitud']= $value["latitud"];
						$todos[$count]['longitud']= $value["longitud"];
						$todos[$count]['direccion']= $value["direccion"];
						$count++;
					}	
					// var_dump($dataProvider);
						$this->render('index',array( 'dataProvider'=>$dataProvider, 'todos'=>$todos,));

					// select objetos.nombre  from objetoetiqueta INNER JOIN objetos on objetoetiqueta.idobjeto = objetos.idobjeto where objetoetiqueta.idetiqueta =

				}

			else{
				Yii::import('ext.gmap.*');
				$dataProvider=new CActiveDataProvider('Objetos', array( 

 'criteria'=>array( 'condition'=>'status=true', ),				 )	);
				$todos= array();
				$count=0;
				// echo("<PRE>");ç
				// var_dump($dataProvider->getData());
				// echo("</PRE>");
				foreach ($dataProvider->getData() as $sitios => $value) {
					$todos[$count]['idobjeto']= $value->idobjeto;
					$todos[$count]['nombre']= $value->nombre;
					$todos[$count]['descripcion']= $value->descripcion;
					$todos[$count]['latitud']= $value->latitud;
					$todos[$count]['longitud']= $value->longitud;
					$count++;
				} 
			$this->render('index',array('dataProvider'=>$dataProvider, 'todos'=>$todos,	));
			}	
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Objetos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Objetos']))
			$model->attributes=$_GET['Objetos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Objetos the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Objetos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Objetos $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='objetos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
