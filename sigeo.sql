<?php 

public function up(){
    
$this->createTable('atributoobjeto',array (
    idatriobje pk, idatributo integer NOT NULL, idobjeto integer NOT NULL, usuariocrea character varying(80), creado timestamp without time zone DEFAULT now(), usuariomodifica character varying(80), actualizado timestamp without time zone DEFAULT now(), ip character varying(80) 
));
}

public function down(){$this->dropTable('atributoobjeto')}

public function up(){
    
$this->createTable('atributos',array ( 
    idatributo pk, nombre character varying(80) NOT NULL, tipo character varying(50) NOT NULL,    caracter character varying(50) NOT NULL,   usuariocrea character varying(80),  creado timestamp without time zone DEFAULT now(),    usuariomodifica character varying(80), actualizado timestamp without time zone DEFAULT now(), ip character varying(80)
));
}

public function down(){$this->dropTable('atributos')}

public function up(){
    
$this->createTable('auth_asignacion',array ( 
    itemname pk, userid character varying(64) NOT NULL, bizrule text, data text 
    ));
}

public function down(){$this->dropTable('auth_asignacion')}

public function up(){
    
$this->createTable('auth_items',array (
    name character varying(64) NOT NULL, type integer NOT NULL, description text, bizrule text, data text
));
}

public function down(){$this->dropTable('auth_items')}

public function up(){
    
$this->createTable('auth_relacion',array (
    parent character varying(64) NOT NULL,    child character varying(64) NOT NULL
));
}

public function down(){$this->dropTable('auth_relacion')}

public function up(){
    
$this->createTable('calificaciones',array (
    idcalificacion pk,     calificacion integer NOT NULL,    idobjeto integer NOT NULL,    idusuario integer NOT NULL,     usuariocrea character varying(80),     creado timestamp without time zone DEFAULT now(),     usuariomodifica character varying(80),     actualizado timestamp without time zone DEFAULT now(),     ip character varying(80)
));
}

public function down(){$this->dropTable('calificaciones')}

public function up(){
    
$this->createTable('categoriaobjeto',array (
    idcateobje pk,     idcategoria integer NOT NULL,    idobjeto integer NOT NULL,    usuariocrea character varying(80),    creado timestamp without time zone DEFAULT now(),    usuariomodifica character varying(80),    actualizado timestamp without time zone DEFAULT now(),    ip character varying(80)
));
}

public function down(){$this->dropTable('categoriaobjeto')}

public function up(){
    
$this->createTable('categorias',array (
    idcategoria pk,     nombre character varying(80) NOT NULL,    usuariocrea character varying(80),    creado timestamp without time zone DEFAULT now(),    usuariomodifica character varying(80),     actualizado timestamp without time zone DEFAULT now(),    ip character varying(80)
));
}

public function down(){$this->dropTable('categorias')}

public function up(){
    
$this->createTable('comentarios',array (
    idcomentario pk,     comentario character varying(600) NOT NULL,    idobjeto integer NOT NULL,    idusuario integer NOT NULL,     usuariocrea character varying(80),    creado timestamp without time zone DEFAULT now(),     usuariomodifica character varying(80),    actualizado timestamp without time zone DEFAULT now(),    ip character varying(80)
));
}

public function down(){$this->dropTable('comentarios')}
public function up(){
    
$this->createTable('documentos',array (
    id pk,     documento character varying(255) NOT NULL,     descripcion text NOT NULL,    idserie integer,    idsubserie integer,    archivo bytea,    usuariocrea character varying(15),    fechacrea timestamp without time zone,    usuariomodifica character varying(15),    fechamodifica timestamp without time zone
));
}

public function down(){$this->dropTable('documentos')}
public function up(){
    
$this->createTable('etiquetas',array (
    idetiqueta pk,     nombre character varying(80) NOT NULL,    usuario character varying(80),    creado timestamp without time zone DEFAULT now(),     usuariomodifica character varying(80),   actualizado timestamp without time zone DEFAULT now(),    ip character varying(80)
));
}

public function down(){$this->dropTable('etiquetas')}
public function up(){
    
$this->createTable('ha_logins',array (
    id pk,     userid integer NOT NULL,     loginprovider character varying(50) NOT NULL,     loginprovideridentifier character varying(100) NOT NULL 
    ));
}

public function down(){$this->dropTable('ha_logins')}
public function up(){
    
$this->createTable('imagen',array (
    id pk,     foto character varying(200) NOT NULL
));
}

public function down(){$this->dropTable('imagen')}
public function up(){
    
$this->createTable('objetoetiqueta',array (
    idobjeetiq pk,     idetiqueta integer NOT NULL,    idobjeto integer NOT NULL,     usuariocrea character varying(80),    creado timestamp without time zone DEFAULT now(),    usuariomodifica character varying(80),    actualizado timestamp without time zone DEFAULT now(),    ip character varying(80)
));
}

public function down(){$this->dropTable('objetoetiqueta')}
public function up(){
    
$this->createTable('objetos',array (
    idobjeto pk,     nombre character varying(80) NOT NULL,     descripcion character varying(300) NOT NULL,     direccion character varying(40) NOT NULL,    latitud character varying(40) NOT NULL,    longitud character varying(40) NOT NULL,     zoom numeric NOT NULL,    usuariocrea character varying(80),     creado timestamp without time zone DEFAULT now(),     usuariomodifica character varying(80),    actualizado timestamp without time zone DEFAULT now(),    ip character varying(80)
));
}

public function down(){$this->dropTable('objetos')}

public function up(){
    
$this->createTable('usuarios',array (
    idusuario pk,    nombre character varying(80) NOT NULL,    password character varying(255) NOT NULL,    email character varying(100) NOT NULL,    username character varying(255) NOT NULL,    session text,    usuariocrea character varying(80),    creado timestamp without time zone DEFAULT now(),    usuariomodifica character varying(80),     actualizado timestamp without time zone DEFAULT now(),    ip character varying(80),    token "char"
));
}

public function down(){$this->dropTable('usuarios')}
public function up(){
    
$this->createTable('visitas',array (
    idvisitas pk,    idobjeto integer NOT NULL,    idusuario integer NOT NULL,    usuario character varying(80),    creado timestamp without time zone DEFAULT now(),    usuariomodifica character varying(80),    actualizado timestamp without time zone DEFAULT now(),    ip character varying(80)
));
}

public function down(){$this->dropTable('visitas')}