-- MySQL dump 10.13  Distrib 5.5.49, for Linux (x86_64)
--
-- Host: localhost    Database: sigeocosas
-- ------------------------------------------------------
-- Server version	5.5.49-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atributoobjeto`
--

DROP TABLE IF EXISTS `atributoobjeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atributoobjeto` (
  `idatriobje` int(11) NOT NULL AUTO_INCREMENT,
  `idatributo` int(11) NOT NULL,
  `idobjeto` int(11) NOT NULL,
  `usuariocrea` varchar(80) DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(80) DEFAULT NULL,
  `actualizado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idatriobje`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atributoobjeto`
--

LOCK TABLES `atributoobjeto` WRITE;
/*!40000 ALTER TABLE `atributoobjeto` DISABLE KEYS */;
/*!40000 ALTER TABLE `atributoobjeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atributos`
--

DROP TABLE IF EXISTS `atributos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atributos` (
  `idatributo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `caracter` varchar(50) NOT NULL,
  `usuariocrea` varchar(80) DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(80) DEFAULT NULL,
  `actualizado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idatributo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atributos`
--

LOCK TABLES `atributos` WRITE;
/*!40000 ALTER TABLE `atributos` DISABLE KEYS */;
/*!40000 ALTER TABLE `atributos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_asignacion`
--

DROP TABLE IF EXISTS `auth_asignacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_asignacion` (
  `itemname` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_asignacion`
--

LOCK TABLES `auth_asignacion` WRITE;
/*!40000 ALTER TABLE `auth_asignacion` DISABLE KEYS */;
INSERT INTO `auth_asignacion` VALUES (1,44,'','N'),(2,61,'','N'),(3,35,'','N');
/*!40000 ALTER TABLE `auth_asignacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_items`
--

DROP TABLE IF EXISTS `auth_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_items` (
  `name` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_items`
--

LOCK TABLES `auth_items` WRITE;
/*!40000 ALTER TABLE `auth_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_relacion`
--

DROP TABLE IF EXISTS `auth_relacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_relacion` (
  `parent` int(11) NOT NULL AUTO_INCREMENT,
  `child` int(11) DEFAULT NULL,
  PRIMARY KEY (`parent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_relacion`
--

LOCK TABLES `auth_relacion` WRITE;
/*!40000 ALTER TABLE `auth_relacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_relacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calificaciones`
--

DROP TABLE IF EXISTS `calificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calificaciones` (
  `idcalificacion` int(11) NOT NULL AUTO_INCREMENT,
  `calificacion` int(11) NOT NULL,
  `idobjeto` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `usuariocrea` varchar(80) DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(80) DEFAULT NULL,
  `actualizado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idcalificacion`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calificaciones`
--

LOCK TABLES `calificaciones` WRITE;
/*!40000 ALTER TABLE `calificaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `calificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoriaobjeto`
--

DROP TABLE IF EXISTS `categoriaobjeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoriaobjeto` (
  `idcateobje` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) NOT NULL,
  `idobjeto` int(11) NOT NULL,
  `usuariocrea` varchar(80) DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(80) DEFAULT NULL,
  `actualizado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idcateobje`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoriaobjeto`
--

LOCK TABLES `categoriaobjeto` WRITE;
/*!40000 ALTER TABLE `categoriaobjeto` DISABLE KEYS */;
INSERT INTO `categoriaobjeto` VALUES (1,19,1,NULL,'2016-08-20 04:50:57',NULL,'0000-00-00 00:00:00',NULL),(2,19,2,NULL,'2016-08-20 05:18:43',NULL,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `categoriaobjeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `usuariocrea` varchar(80) DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(80) DEFAULT NULL,
  `actualizado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (18,'Religion','jaco','2016-02-09 12:47:08','jaco','2016-02-09 12:47:08','::1'),(17,'Educacion','jaco','2016-02-09 12:45:18','jaco','2016-02-09 12:47:27','::1'),(19,'Diversion','jaco','2016-02-09 12:47:39','jaco','2016-02-09 12:47:39','::1'),(20,'Atenciónciudadana','jaco','2016-02-09 12:47:49','jaco','2016-02-09 12:47:49','::1'),(21,'Señalestransito','jaco','2016-02-09 12:48:50','jaco','2016-02-09 12:48:50','::1');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comentarios`
--

DROP TABLE IF EXISTS `comentarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentarios` (
  `idcomentario` int(11) NOT NULL AUTO_INCREMENT,
  `comentario` varchar(600) NOT NULL,
  `idobjeto` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `usuariocrea` varchar(80) DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(80) DEFAULT NULL,
  `actualizado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idcomentario`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentarios`
--

LOCK TABLES `comentarios` WRITE;
/*!40000 ALTER TABLE `comentarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentos`
--

DROP TABLE IF EXISTS `documentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documento` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `idserie` int(11) DEFAULT NULL,
  `idsubserie` int(11) DEFAULT NULL,
  `archivo` varchar(255) DEFAULT NULL,
  `usuariocrea` varchar(15) DEFAULT NULL,
  `fechacrea` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(15) DEFAULT NULL,
  `fechamodifica` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentos`
--

LOCK TABLES `documentos` WRITE;
/*!40000 ALTER TABLE `documentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etiquetas`
--

DROP TABLE IF EXISTS `etiquetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etiquetas` (
  `idetiqueta` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `usuario` varchar(80) DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(80) DEFAULT NULL,
  `actualizado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idetiqueta`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etiquetas`
--

LOCK TABLES `etiquetas` WRITE;
/*!40000 ALTER TABLE `etiquetas` DISABLE KEYS */;
INSERT INTO `etiquetas` VALUES (5,'compras','jaco','0000-00-00 00:00:00','2016-02-09 06:29:27','2016-02-09 13:29:27','::1'),(6,'juegos','jaco','0000-00-00 00:00:00','2016-02-09 06:29:35','2016-02-09 13:29:35','::1'),(7,'recreacion','jaco','0000-00-00 00:00:00','2016-02-09 06:29:44','2016-02-09 13:29:44','::1'),(8,'deporte','jaco','0000-00-00 00:00:00','2016-02-09 06:29:52','2016-02-09 13:29:52','::1'),(9,'transito','jaco','0000-00-00 00:00:00','2016-02-09 06:30:06','2016-02-09 13:30:06','::1'),(10,'salud','jaco','0000-00-00 00:00:00','2016-02-09 06:30:32','2016-02-09 13:30:32','::1');
/*!40000 ALTER TABLE `etiquetas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ha_logins`
--

DROP TABLE IF EXISTS `ha_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ha_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `loginprovider` varchar(50) NOT NULL,
  `loginprovideridentifier` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ha_logins`
--

LOCK TABLES `ha_logins` WRITE;
/*!40000 ALTER TABLE `ha_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `ha_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagen`
--

DROP TABLE IF EXISTS `imagen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagen`
--

LOCK TABLES `imagen` WRITE;
/*!40000 ALTER TABLE `imagen` DISABLE KEYS */;
/*!40000 ALTER TABLE `imagen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objetoetiqueta`
--

DROP TABLE IF EXISTS `objetoetiqueta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objetoetiqueta` (
  `idobjeetiq` int(11) NOT NULL AUTO_INCREMENT,
  `idetiqueta` int(11) NOT NULL,
  `idobjeto` int(11) NOT NULL,
  `usuariocrea` varchar(80) DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(80) DEFAULT NULL,
  `actualizado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idobjeetiq`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objetoetiqueta`
--

LOCK TABLES `objetoetiqueta` WRITE;
/*!40000 ALTER TABLE `objetoetiqueta` DISABLE KEYS */;
INSERT INTO `objetoetiqueta` VALUES (1,6,1,NULL,'2016-08-20 04:48:24',NULL,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `objetoetiqueta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objetos`
--

DROP TABLE IF EXISTS `objetos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objetos` (
  `idobjeto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `direccion` varchar(40) NOT NULL,
  `latitud` varchar(40) NOT NULL,
  `longitud` varchar(40) NOT NULL,
  `zoom` decimal(10,0) NOT NULL,
  `usuariocrea` varchar(80) DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(80) DEFAULT NULL,
  `actualizado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) DEFAULT '1',
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idobjeto`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objetos`
--

LOCK TABLES `objetos` WRITE;
/*!40000 ALTER TABLE `objetos` DISABLE KEYS */;
INSERT INTO `objetos` VALUES (1,'prueba','prueba2','prueba2','5.452117138540118','-74.66592474257811',14,NULL,'2016-08-20 04:39:08',NULL,'0000-00-00 00:00:00',1,NULL),(2,'prueba2','prueba','prueba','5.446477914573872','-74.66729803359374',13,NULL,'2016-08-20 05:18:13',NULL,'0000-00-00 00:00:00',1,NULL),(3,'Prueba ','Prueba crear ','Calle 11','5.450494331030004','-74.66420744970702',15,NULL,'2016-08-20 22:47:42',NULL,'0000-00-00 00:00:00',1,NULL);
/*!40000 ALTER TABLE `objetos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
INSERT INTO `tbl_migration` VALUES ('m000000_000000_base',1470982666),('m160811_235346_all',1470982671),('m160812_002255_documentos',1470982671),('m160812_002255_etiquetas',1470982671),('m160812_002255_ha_logins',1470982671),('m160812_002255_imagen',1470982671),('m160812_002256_atributoobjeto',1470982671),('m160812_002256_auth_asignacion',1470982671),('m160812_002256_auth_items',1470982671),('m160812_002256_visitas',1470982671),('m160812_002257_atributos',1470982671),('m160812_002257_auth_relacion',1470982671),('m160812_002257_categoriaobjeto',1470982671),('m160812_002257_comentarios',1470982671),('m160812_002258_calificaciones',1470982671),('m160812_002258_categorias',1470982671),('m160812_002258_objetoetiqueta',1470982671),('m160812_002258_usuarios',1470982671),('m160812_002259_objeto',1470982671),('m160812_033911_datos',1470982671);
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(255) NOT NULL,
  `session` text,
  `usuariocrea` varchar(80) DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(80) DEFAULT NULL,
  `actualizado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (61,'jaco','4cb1e66a2da66ac0644061dd27350d31','jaco','jaco','55f1f827342547.12672295','sara','2015-09-11 04:37:43','sara','2015-09-11 04:37:43','::1');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitas`
--

DROP TABLE IF EXISTS `visitas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitas` (
  `idvisitas` int(11) NOT NULL AUTO_INCREMENT,
  `idobjeto` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `usuario` varchar(80) DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuariomodifica` varchar(80) DEFAULT NULL,
  `actualizado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idvisitas`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitas`
--

LOCK TABLES `visitas` WRITE;
/*!40000 ALTER TABLE `visitas` DISABLE KEYS */;
INSERT INTO `visitas` VALUES (1,22,61,NULL,'2016-08-12 22:18:54',NULL,'0000-00-00 00:00:00','186.170.234.150'),(2,22,61,NULL,'2016-08-12 22:19:39',NULL,'0000-00-00 00:00:00','186.170.234.150'),(3,1,1,NULL,'2016-08-20 05:10:11',NULL,'0000-00-00 00:00:00','190.142.102.119'),(4,1,1,NULL,'2016-08-20 05:13:13',NULL,'0000-00-00 00:00:00','190.142.102.119'),(5,1,1,NULL,'2016-08-20 05:42:59',NULL,'0000-00-00 00:00:00','190.142.102.119'),(6,1,1,NULL,'2016-08-20 05:43:59',NULL,'0000-00-00 00:00:00','190.142.102.119'),(7,1,1,NULL,'2016-08-20 05:44:24',NULL,'0000-00-00 00:00:00','190.142.102.119'),(8,1,1,NULL,'2016-08-20 05:44:31',NULL,'0000-00-00 00:00:00','190.142.102.119'),(9,1,1,NULL,'2016-08-20 05:45:16',NULL,'0000-00-00 00:00:00','190.142.102.119'),(10,1,1,NULL,'2016-08-20 05:45:32',NULL,'0000-00-00 00:00:00','190.142.102.119'),(11,1,1,NULL,'2016-08-20 05:49:41',NULL,'0000-00-00 00:00:00','190.142.102.119'),(12,1,1,NULL,'2016-08-20 05:51:33',NULL,'0000-00-00 00:00:00','190.142.102.119'),(13,1,1,NULL,'2016-08-20 05:52:04',NULL,'0000-00-00 00:00:00','190.142.102.119'),(14,2,1,NULL,'2016-08-20 06:17:37',NULL,'0000-00-00 00:00:00','190.142.102.119'),(15,2,1,NULL,'2016-08-20 06:19:24',NULL,'0000-00-00 00:00:00','190.142.102.119'),(16,2,1,NULL,'2016-08-20 06:19:57',NULL,'0000-00-00 00:00:00','190.142.102.119'),(17,2,1,NULL,'2016-08-20 06:22:01',NULL,'0000-00-00 00:00:00','190.142.102.119'),(18,1,1,NULL,'2016-08-20 07:23:47',NULL,'0000-00-00 00:00:00','190.142.102.119'),(19,2,1,NULL,'2016-08-20 07:29:45',NULL,'0000-00-00 00:00:00','190.142.102.119'),(20,2,1,NULL,'2016-08-20 07:29:54',NULL,'0000-00-00 00:00:00','190.142.102.119'),(21,2,1,NULL,'2016-08-20 07:30:15',NULL,'0000-00-00 00:00:00','190.142.102.119'),(22,2,1,NULL,'2016-08-20 07:30:18',NULL,'0000-00-00 00:00:00','190.142.102.119'),(23,2,1,NULL,'2016-08-20 07:30:27',NULL,'0000-00-00 00:00:00','190.142.102.119'),(24,2,1,NULL,'2016-08-20 07:30:59',NULL,'0000-00-00 00:00:00','190.142.102.119'),(25,2,1,NULL,'2016-08-20 07:32:35',NULL,'0000-00-00 00:00:00','190.142.102.119'),(26,2,1,NULL,'2016-08-20 07:32:44',NULL,'0000-00-00 00:00:00','190.142.102.119'),(27,2,1,NULL,'2016-08-20 07:32:47',NULL,'0000-00-00 00:00:00','190.142.102.119'),(28,2,1,NULL,'2016-08-20 07:33:15',NULL,'0000-00-00 00:00:00','190.142.102.119'),(29,2,1,NULL,'2016-08-20 07:33:23',NULL,'0000-00-00 00:00:00','190.142.102.119'),(30,2,1,NULL,'2016-08-20 07:33:32',NULL,'0000-00-00 00:00:00','190.142.102.119'),(31,2,1,NULL,'2016-08-20 07:33:53',NULL,'0000-00-00 00:00:00','190.142.102.119'),(32,2,1,NULL,'2016-08-20 07:34:37',NULL,'0000-00-00 00:00:00','190.142.102.119'),(33,2,1,NULL,'2016-08-20 07:34:52',NULL,'0000-00-00 00:00:00','190.142.102.119'),(34,2,1,NULL,'2016-08-20 07:35:45',NULL,'0000-00-00 00:00:00','190.142.102.119'),(35,2,1,NULL,'2016-08-20 07:35:57',NULL,'0000-00-00 00:00:00','190.142.102.119'),(36,2,1,NULL,'2016-08-20 07:37:29',NULL,'0000-00-00 00:00:00','190.142.102.119'),(37,1,1,NULL,'2016-08-20 20:21:09',NULL,'0000-00-00 00:00:00','190.142.102.119'),(38,3,1,NULL,'2016-08-20 23:13:40',NULL,'0000-00-00 00:00:00','190.68.178.145'),(39,2,1,NULL,'2016-08-27 03:41:23',NULL,'0000-00-00 00:00:00','190.142.102.119'),(40,2,1,NULL,'2016-08-27 04:05:25',NULL,'0000-00-00 00:00:00','190.142.102.119');
/*!40000 ALTER TABLE `visitas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-26 21:07:32
